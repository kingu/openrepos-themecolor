## TODOs:

 * [] clean up and centralize functions and interfaces
 * [] consolidate shelves
 * [] taking a completely empty shelf to lab resets all colors to Ambience default. use this somehow?
 * [] export ambience file: support other colors

## ROADMAP:

### 3.0+
 * [] support "plugins" for Generators (Name, algorithm)

### 2.8
 * [] externalize functions more, for:
 * [] listen on dbus ambience changed signal and apply scheme from top shelf if found
 * [] apply scheme from top shelf on start

### 2.7
 * [x] export ambience file/ generate ambience RPM: write shell script + desktop link, open on done
 * [] support editing transparency (https://openrepos.net/comment/37225#comment-37225)
 * [x] support writing highlightDimmerColor
 * [] support bg color (remorse) <-- not useful
 * [x] swapper: add missing colors
 * [x] generator: add missing colors
 * [x] Generators: externalize all functions in external file, use "tint" and "darken" in generators
 * [x] steal Flow layout idea from BatteryBuddy for landscape
 * [x] correct shelves saved as dimgray, as taking them to lab makes it uses the dimgray instead of undefined

## DONE:

### pre 2.7
 * [x] add reset to default option (https://openrepos.net/comment/37079#comment-37079)
 * [x] autocompute theme
 * [x] handle ambience changed dbus signal
 * [x] save slots
 * [x] support backgroundGlowColor
