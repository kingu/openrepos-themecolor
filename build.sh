#!/bin/sh

./update_translations.sh qm || exit 1
printf 'specifying...\n'
specify -Nns rpm/*yaml || exit 1
printf 'linting...\n'
for f in $(find qml -name "*.qml" -o -name "*.js" ); do
 /usr/lib/qt5/bin/qmllint "$f" || exit 1
done
printf 'building...\n'
rpmbuild -bb --build-in-place rpm/*.spec > build.log 2>&1
printf "exit: $?\n"
grep ^Wrote build.log

