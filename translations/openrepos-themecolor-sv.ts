<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AppInfoSingleton</name>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="10"/>
        <source>ThemeColor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="11"/>
        <source>ThemeColor® RPM Builder™</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>(the # is optional)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="15"/>
        <source>Tint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="15"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="23"/>
        <source>Darken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="31"/>
        <source>Brighten</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="10"/>
        <source>Randomizer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="12"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Scheme Generators</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="16"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="21"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="45"/>
        <source>Generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="40"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <source>Solarize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Generate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Highlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <source>Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Summer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="16"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="23"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="31"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="38"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="51"/>
        <source>Applying</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="42"/>
        <source>Adjust sliders, tap to reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation type="unfinished">Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="20"/>
        <source>Secondary Color</source>
        <translation type="unfinished">Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="25"/>
        <source>Highlight Color</source>
        <translation type="unfinished">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="30"/>
        <source>Secondary Highlight Color</source>
        <translation type="unfinished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="67"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="67"/>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="87"/>
        <location filename="../qml/components/ColorSwapper.qml" line="113"/>
        <source>Primary Color</source>
        <translation type="unfinished">Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="88"/>
        <location filename="../qml/components/ColorSwapper.qml" line="114"/>
        <source>Secondary Color</source>
        <translation type="unfinished">Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="89"/>
        <location filename="../qml/components/ColorSwapper.qml" line="115"/>
        <source>Highlight Color</source>
        <translation type="unfinished">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="90"/>
        <location filename="../qml/components/ColorSwapper.qml" line="116"/>
        <source>Secondary Highlight Color</source>
        <translation type="unfinished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="117"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInput.qml" line="31"/>
        <source>Color input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorTextInput.qml" line="74"/>
        <source>Input value, tap to reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="15"/>
        <source>Primary Color</source>
        <translation type="unfinished">Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="20"/>
        <source>Secondary Color</source>
        <translation type="unfinished">Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="25"/>
        <source>Highlight Color</source>
        <translation type="unfinished">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="30"/>
        <source>Secondary Highlight Color</source>
        <translation type="unfinished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="35"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="22"/>
        <source>ThemeColor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="30"/>
        <source>Adjust Theme Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Current Colour Model:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="48"/>
        <source>Showroom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="86"/>
        <source>Laboratory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="99"/>
        <source>Input Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="100"/>
        <source>Tap to switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">grey</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Apply colours to system</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Reload colors from system</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="108"/>
        <source>Swapper/Copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="165"/>
        <source>Applying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="164"/>
        <source>Apply Colors to System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Reload Colours from System</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="109"/>
        <source>Generators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="167"/>
        <source>Reload Colors from current Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="170"/>
        <source>Reload Colors from System Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="176"/>
        <source>Experimental or dangerous actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="177"/>
        <source>Export to Ambience package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="180"/>
        <source>Edit Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>(not implemented)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>Save Theme to current Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="184"/>
        <source>Saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="188"/>
        <location filename="../qml/pages/FirstPage.qml" line="191"/>
        <source>Resetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="194"/>
        <source>Restarting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">Saving…</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="187"/>
        <source>Reset all values and restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="190"/>
        <source>Reset nonstandard values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="193"/>
        <source>Restart Lipstick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="158"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="106"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="107"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="110"/>
        <source>Jolla Original</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="161"/>
        <source>Open Ambience Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Apply colours to current Theme</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Reload Colours</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Reset Colours</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="19"/>
        <source>How to Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="35"/>
        <source>The Showroom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="41"/>
        <source>The Laboratory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation type="vanished">In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.
          </source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;Check what your theme will look like in the Showroom display.&lt;br /&gt; &lt;br /&gt; When you&apos;re done, use the PullDown Menu.
          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="50"/>
        <source>The Cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt; Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="57"/>
        <source>Tips and Caveats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="37"/>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="43"/>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="52"/>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Copyright:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Source Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="80"/>
        <source>Translations:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard. If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Version: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Copyright: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Licence: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Source Code: </translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="22"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>ThemeColor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="26"/>
        <source>A Lootbox was delivered!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>now has more shelves!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>Your persistence has been rewarded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>Your persistence has been rewarded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="38"/>
        <source>Purchase Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="47"/>
        <source>Payment* received!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="47"/>
        <source>Buy more shelves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="65"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="75"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="66"/>
        <source>Export Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="76"/>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="80"/>
        <source>Export to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="85"/>
        <source>Ambience Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="87"/>
        <source>A cool Ambience Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="109"/>
        <source>Click to export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="39"/>
        <source>Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="39"/>
        <source>Shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="62"/>
        <source>Take to Lab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="63"/>
        <source>Put on Shelf</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="82"/>
        <source>Global Cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="114"/>
        <source>Clean out this cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="114"/>
        <source>Spring Clean</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="13"/>
        <source>anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="113"/>
        <source>Ambience Cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="157"/>
        <source>Clean out this cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="157"/>
        <source>Spring Clean</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="obsolete">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="obsolete">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="obsolete">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="obsolete">Secondary Highlight Colour</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="27"/>
        <source>Text Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="28"/>
        <source>UI Elements</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="18"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="18"/>
        <source>Showroom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="36"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="92"/>
        <source>Primary Color</source>
        <translation type="unfinished">Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="21"/>
        <source>A very long line showing Text in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="42"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="70"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="98"/>
        <source>Secondary Color</source>
        <translation type="unfinished">Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <source>Highlight Color</source>
        <translation type="unfinished">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>Secondary Highlight Color</source>
        <translation type="unfinished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="21"/>
        <source>Error Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="36"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="42"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="70"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="92"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="98"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="36"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="42"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="70"/>
        <source>Overlay Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="92"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="98"/>
        <source>Dim Highlight Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="31"/>
        <source>Progress Bar Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="34"/>
        <source>Remorse Item Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="90"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="98"/>
        <source>MenuItem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <source>selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="98"/>
        <source>disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="118"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="119"/>
        <source>Button</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="41"/>
        <source>Edit Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="48"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="72"/>
        <source>Highlight Background Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="73"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="78"/>
        <source>Color Alpha Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="79"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="80"/>
        <source>Secondary Color</source>
        <translation type="unfinished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="obsolete">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="82"/>
        <source>Secondary Highlight Color</source>
        <translation type="unfinished">Secondary Highlight Colour</translation>
    </message>
</context>
</TS>
