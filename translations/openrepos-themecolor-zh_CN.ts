<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AppInfoSingleton</name>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="10"/>
        <source>ThemeColor</source>
        <translation>主题颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="11"/>
        <source>ThemeColor® RPM Builder™</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation>定义 RGB 或 aRGB 值，例如</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>(the # is optional)</source>
        <translation>(井号 # 可选)</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="15"/>
        <source>Tint</source>
        <translation>渲染</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="15"/>
        <source>Red</source>
        <translation>红</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="23"/>
        <source>Darken</source>
        <translation>降低亮度</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="31"/>
        <source>Brighten</source>
        <translation>提高亮度</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="10"/>
        <source>Randomizer</source>
        <translation>随机生成程序</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="12"/>
        <source>Filters</source>
        <translation>过滤器</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Scheme Generators</source>
        <translation>方案生成程序</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="16"/>
        <source>Random</source>
        <translation>随机</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="21"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="45"/>
        <source>Generated</source>
        <translation>已生成</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Bright</source>
        <translation>亮色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="40"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <source>Solarize</source>
        <translation>曝光</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Generate</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>from</source>
        <translation>来自</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Highlight</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <source>Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Summer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="16"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="23"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="31"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="38"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="51"/>
        <source>Applying</source>
        <translation>应用</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">间色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">第二强调色</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="42"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>调整滑块，点击以重置</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation>原色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="20"/>
        <source>Secondary Color</source>
        <translation>次色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="25"/>
        <source>Highlight Color</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="30"/>
        <source>Secondary Highlight Color</source>
        <translation>次强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Highlight Background Color</source>
        <translation>背景强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation>背景发光色</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="67"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="67"/>
        <source>Swap</source>
        <translation>交换程序</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="87"/>
        <location filename="../qml/components/ColorSwapper.qml" line="113"/>
        <source>Primary Color</source>
        <translation>原色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="88"/>
        <location filename="../qml/components/ColorSwapper.qml" line="114"/>
        <source>Secondary Color</source>
        <translation>次色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="89"/>
        <location filename="../qml/components/ColorSwapper.qml" line="115"/>
        <source>Highlight Color</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="90"/>
        <location filename="../qml/components/ColorSwapper.qml" line="116"/>
        <source>Secondary Highlight Color</source>
        <translation>次强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="117"/>
        <source>Highlight Background Color</source>
        <translation>背景强调色</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInput.qml" line="31"/>
        <source>Color input</source>
        <translation>颜色输入</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorTextInput.qml" line="74"/>
        <source>Input value, tap to reset</source>
        <translation>输入值，点击以重置</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="15"/>
        <source>Primary Color</source>
        <translation>原色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="20"/>
        <source>Secondary Color</source>
        <translation>次色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="25"/>
        <source>Highlight Color</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="30"/>
        <source>Secondary Highlight Color</source>
        <translation>次强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="35"/>
        <source>Highlight Background Color</source>
        <translation>背景强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation>背景发光色</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="22"/>
        <source>ThemeColor</source>
        <translation>主题颜色</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="30"/>
        <source>Adjust Theme Colors</source>
        <translation>调整主题颜色</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">当前颜色模型:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">间色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">第二强调色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="48"/>
        <source>Showroom</source>
        <translation>陈列室</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="86"/>
        <source>Laboratory</source>
        <translation>实验室</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="99"/>
        <source>Input Mode:</source>
        <translation>输入模式：</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="100"/>
        <source>Tap to switch</source>
        <translation>点击以切换</translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">灰色</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">应用颜色到系统</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">重新加载系统颜色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="164"/>
        <source>Apply Colors to System</source>
        <translation>应用颜色到系统</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">重新加载系统颜色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="108"/>
        <source>Swapper/Copier</source>
        <translation>交换程序/复印机</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">根据高光计算所有颜色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="109"/>
        <source>Generators</source>
        <translation>生成程序</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="165"/>
        <source>Applying</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="167"/>
        <source>Reload Colors from current Theme</source>
        <translation>从当前主题重新加载颜色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="170"/>
        <source>Reload Colors from System Config</source>
        <translation>从系统配置中重新加载颜色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="176"/>
        <source>Experimental or dangerous actions</source>
        <translation>实验性及危险性操作</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">输出到氛围文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="177"/>
        <source>Export to Ambience package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="180"/>
        <source>Edit Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>(not implemented)</source>
        <translation>(尚未实施)</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>Save Theme to current Ambience</source>
        <translation>保存主题到当前氛围</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="184"/>
        <source>Saving</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="188"/>
        <location filename="../qml/pages/FirstPage.qml" line="191"/>
        <source>Resetting</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="194"/>
        <source>Restarting</source>
        <translation>正在重启</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">正在保存…</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="187"/>
        <source>Reset all values and restart</source>
        <translation>重置全部值并重启</translation>
    </message>
    <message>
        <source>Remove custom values from configuration</source>
        <translation type="vanished">从配置中移除自定义值</translation>
    </message>
    <message>
        <source>Resetting...</source>
        <translation type="vanished">正在重置…</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="190"/>
        <source>Reset nonstandard values</source>
        <translation>重置非标准值</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="193"/>
        <source>Restart Lipstick</source>
        <translation>重启 Lipstick</translation>
    </message>
    <message>
        <source>Restarting...</source>
        <translation type="vanished">正在重置…</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="158"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="106"/>
        <source>Sliders</source>
        <translation>滑块</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="107"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">对换程序</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">随机生成器</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="110"/>
        <source>Jolla Original</source>
        <translation>Jolla 原生</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="161"/>
        <source>Open Ambience Settings</source>
        <translation>打开氛围设置</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">应用颜色到当前主题</translation>
    </message>
    <message>
        <source>Applying...</source>
        <translation type="vanished">正在应用…</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">重新加载颜色</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">重置颜色</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">使用下方部分滑块以调整颜色。&lt;br /&gt;            满意之后，点击滑块上方区域以设置颜色。          请在陈列室检查主题显示效果。&lt;br /&gt;            &lt;br /&gt;            当你完成之后，请使用上拉菜单以应用你的创作。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="19"/>
        <source>How to Use</source>
        <translation>如何使用</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;



          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;



          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">该应用程序允许你修改当前Lipstick配色方案。它不会改变或创建新氛围。当你修改氛围、Lipstick重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br /&gt;



当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。lt;br/&gt;&lt;br/&gt;



我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;



          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;



          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">应用程序允许你修改当前Lipstick配色方案。它不会改变或创建新氛围。当你修改氛围、Lipstick重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br /&gt;



当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。lt;br/&gt;&lt;br/&gt;



我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;

          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;

          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">应用程序允许你修改当前 Lipstick 配色方案。它不会改变或创建新氛围。当你修改氛围、Lipstick重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br /&gt;



当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。lt;br/&gt;&lt;br/&gt;



我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="35"/>
        <source>The Showroom</source>
        <translation>陈列室</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">首页的顶部区域 (&quot;陈列室&quot;) 不可交互，仅用于显示当前选择的颜色。&lt;br /&gt;  你可以在此预览你的创作。    </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="41"/>
        <source>The Laboratory</source>
        <translation>实验室</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.

          </source>
        <translation type="vanished">在滑块输出模式，使用下方滑块以调整颜色。 在文本输出模式，你可以直接输入颜色值。随机生成程序名副其实，Jolla 原生就像系统设置那样。&lt;br /&gt;  在陈列室检查主题效果。&lt;br /&gt;            &lt;br /&gt; 当你完成之后，请使用下拉菜单。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="57"/>
        <source>Tips and Caveats</source>
        <translation>建议及附加说明</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;

          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">创建的主题颜色可能使UI的某些部分不可见，特别是不明显的区域，例如虚拟键盘。&lt;br /&gt;

推荐你保存已知良好的颜色方案于壁橱，这样你以后就可以轻松恢复。&lt;br /&gt;          &lt;br /&gt; 如果你完全搞乱了颜色，请使用下拉菜单以重置全部，或使用命令行。 &lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          并且重复储存在此的其它全部颜色。 &lt;br /&gt;  从系统设置修改氛围可能也有用。</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">首页顶部区域（陈列室）不可交互，仅用于显示当前选取的颜色。&lt;br /&gt;你可以在此预览你的创作。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation>该软件允许你修改当前 Lipstick 配色方案。它不会改变或创建新氛围。当你修改氛围、 Lipstick 重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br/&gt;
当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。&lt;br/&gt;
我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="37"/>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation>首页顶部区域（陈列室）显示当前选择用于各种UI元素的颜色。&lt;br /&gt;你可以在此预览你的创作。&lt;br /&gt;点击任意区域将会隐藏。如果想要取消隐藏，请点击标题标头。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="43"/>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation>在滑块输出模式，使用下方滑块以调整颜色。 在文本输出模式，你可以直接输入颜色值。随机生成程序名副其实，Jolla 原生就像系统设置那样。&lt;br /&gt;在陈列室检查主题效果。&lt;br /&gt;&lt;br /&gt; 当你完成之后，请使用下拉菜单。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="52"/>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation>该区域允许你储存你创建的调色板以便之后重复使用。 这里有个全局壁橱，还有一个特别针对当前氛围。&lt;br /&gt; 请注意只有系统氛围拥有名字。自定义氛围目前会以匿名形式显示。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation>Jolla 氛围只定义了四种颜色：原色（根据氛围方案是深色还是亮色）、次色（主色不透明一点）、加高光和次高光。这些都是在氛围设置中选择的颜色。
该软件允许你编辑这四种颜色以外的其它颜色，但它们不会受到氛围修改和 Lipstick 重启的影响，因为它们存储在 dconf 数据库中并保持不变。这意味着一旦通过软件应用，它们将始终保持不变，直到你在软件中再次更改它们。如果有必要，你可以使用下面文档中的重置选项来摆脱它们。
&lt;br /&gt;
当切换编辑模式、将颜色应用到系统或从壁橱中提取调色板时，该软件经常会对颜色感到困惑。如果发生这种情况，请尝试从系统中重新加载，这在大多数情况下是有帮助的。&lt;br /&gt;
&lt;br /&gt;
创建颜色方案可能会使部分交换界面无法阅读。特别是对于一些不明显的区域，比如虚拟键盘。&lt;br /&gt;
在橱柜里存放一个已知好的配色方案，这样你就可以很方便地去还原。
&lt;br /&gt;
如果你把颜色弄乱了，请使用上拉菜单选项来重置一切，或者使用命令行：&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt; 。
并重复应用于存储的所有其它颜色。&lt;br /&gt;
从 “系统设置 ”中更换氛围也可能会有用。</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Copyright:</source>
        <translation>版权：</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>License:</source>
        <translation>许可证 :</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Source Code:</source>
        <translation>源代码：</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="80"/>
        <source>Translations:</source>
        <translation>翻译：</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">版权：</translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">许可证：</translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">源代码：</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;



            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.



          </source>
        <translation type="vanished">在滑块输出模式，使用下方滑块以调整颜色。&lt;br /&gt;            在文本模式，你可以直接输入颜色值。&lt;br /&gt;



            在陈列室检查你的方案效果。&lt;br /&gt;            &lt;br /&gt;  当你完成之后，你可以通过上拉菜单应用你的当前创作。          



          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="50"/>
        <source>The Cupboard</source>
        <translation>壁橱</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">该区域允许你储存你创建的调色板以便之后重复使用。 这里有个全局壁橱，特别针对当前氛围。&lt;br /&gt; 请注意只有系统氛围拥有名字。自定义氛围目前会以匿名形式显示。</translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="22"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>ThemeColor</source>
        <translation>主题颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="26"/>
        <source>A Lootbox was delivered!</source>
        <translation>已送出 Lootbox！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>now has more shelves!</source>
        <translation>现在拥有很多架子！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>Your persistence has been rewarded.</source>
        <translation>你的坚持不懈得到了回报。</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>Your persistence has been rewarded!</source>
        <translation>你的坚持不懈得到了回报！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="38"/>
        <source>Purchase Options</source>
        <translation>购买选项</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="47"/>
        <source>Payment* received!</source>
        <translation>已收到支付！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="47"/>
        <source>Buy more shelves</source>
        <translation>购买更多架子</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>使用 Jolla 商店凭证以购买储存Lootbox。</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="65"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>感谢你的购买！你的额外架子会在下次更新送出！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="75"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) 该软件没有内购服务，Jolla 商店也没有 Lootbox 。这只是个玩笑。&lt;br /&gt;你并没有捐赠软件。&lt;br /&gt;实际上什么也没有发生，不是吗？</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) 该软件没有内购服务，Jolla 商店也没有 Lootbox 。这只是个玩笑。&lt;br /&gt;你没有捐赠软件。&lt;br /&gt;实际上什么也没有发生 不是吗？</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="66"/>
        <source>Export Functions</source>
        <translation>导出功能</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
                     </source>
        <translation type="vanished">你可以导出你的创作到.ambience（json）文件。&lt;br /&gt;
虽然该功能目前还不是很有用，但你可以使用该文件以构建你自己的氛围，通过添加壁纸、及某些声音操作，然后打包所有东西为一个RPM文件。
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="76"/>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="80"/>
        <source>Export to File</source>
        <translation>导出到文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="85"/>
        <source>Ambience Name</source>
        <translation>氛围名称</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="87"/>
        <source>A cool Ambience Name</source>
        <translation>给你的氛围取个好名字</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaveAmbience.qml" line="109"/>
        <source>Click to export</source>
        <translation>点击以导出</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="39"/>
        <source>Ambience</source>
        <translation>氛围</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="39"/>
        <source>Shelf</source>
        <translation>架子</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="62"/>
        <source>Take to Lab</source>
        <translation>放入实验室</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="63"/>
        <source>Put on Shelf</source>
        <translation>放上架子</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="82"/>
        <source>Global Cupboard</source>
        <translation>全部壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="114"/>
        <source>Clean out this cupboard</source>
        <translation>清空该壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="114"/>
        <source>Spring Clean</source>
        <translation>春季扫除</translation>
    </message>
    <message>
        <source>Spring Clean...</source>
        <translation type="vanished">春季清洁…</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="13"/>
        <source>anonymous</source>
        <translation>匿名</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="113"/>
        <source>Ambience Cupboard</source>
        <translation>氛围壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="157"/>
        <source>Clean out this cupboard</source>
        <translation>清空该壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="157"/>
        <source>Spring Clean</source>
        <translation>春季扫除</translation>
    </message>
    <message>
        <source>Spring Clean...</source>
        <translation type="vanished">春季清除…</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">显示文本的长区域</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">次色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">次强调色</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">错误颜色</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">背景色</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">四种背景涂层不透明度及颜色</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">进度条演示</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">点击以重启演示</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">擦除项目演示</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">菜单项</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">已选择</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">已禁用</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">按钮</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="27"/>
        <source>Text Elements</source>
        <translation>文本元素</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="28"/>
        <source>UI Elements</source>
        <translation>UI元素</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="18"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="18"/>
        <source>Showroom</source>
        <translation type="unfinished">陈列室</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">显示文本的长区域</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="36"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="92"/>
        <source>Primary Color</source>
        <translation>原色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="21"/>
        <source>A very long line showing Text in</source>
        <translation>显示文本的长区域</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="42"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="70"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="98"/>
        <source>Secondary Color</source>
        <translation>间色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <source>Highlight Color</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>Secondary Highlight Color</source>
        <translation>第二强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="21"/>
        <source>Error Color</source>
        <translation>错误颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="36"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="42"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="70"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="92"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="98"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="36"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="42"/>
        <source>Highlight Background Color</source>
        <translation>背景强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="70"/>
        <source>Overlay Background Color</source>
        <translation>涂层背景色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="92"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="98"/>
        <source>Dim Highlight Color</source>
        <translation>昏暗强调色</translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="31"/>
        <source>Progress Bar Demo</source>
        <translation>进度条演示</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="34"/>
        <source>Remorse Item Demo</source>
        <translation>擦除项目演示</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="90"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="98"/>
        <source>MenuItem</source>
        <translation>菜单项</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <source>selected</source>
        <translation>已选择</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="98"/>
        <source>disabled</source>
        <translation>已禁用</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="118"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="119"/>
        <source>Button</source>
        <translation>按钮</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="41"/>
        <source>Edit Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="48"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="72"/>
        <source>Highlight Background Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="73"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="78"/>
        <source>Color Alpha Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="79"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="80"/>
        <source>Secondary Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="obsolete">强调色</translation>
    </message>
    <message>
        <location filename="../qml/pages/TransparencyEditor.qml" line="82"/>
        <source>Secondary Highlight Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="obsolete">背景强调色</translation>
    </message>
</context>
</TS>
