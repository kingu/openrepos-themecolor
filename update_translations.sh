#!/bin/sh

BASENAMES=openrepos-themecolor
LANGS="en_IE de nb_NO zh_CN es sv"

#RELEASE_OPT = -nounfinished -silent -compress -idbased -markuntranslated !
# do not use -compress, this does not work!
#RELEASE_OPT = -idbased
RELEASE_OPT="-nounfinished -removeidentical"

if [ -z $1 ]; then  echo give ts and/or qm as parameter ; exit 1 ; fi
for f in "${BASENAMES}"; do
  for l in ${LANGS}; do
  [ "$1" == "ts" ] && ( lupdate -recursive qml -target-language "$l" -ts translations/"${f}"-"${l}".ts  || exit 1 )
  [ "$2" == "qm" ] && ( lrelease ${RELEASE_OPT} translations/"${f}"-"${l}".ts || exit 1 )
  done
  [ "$1" == "ts" ] && ( sed 's/en_IE/en_GB/' translations/"${f}-en_IE.ts" >  translations/"${f}-en_GB.ts" || exit 1 )
  [ "$1" == "qm" ] && ( lrelease ${RELEASE_OPT} translations/"${f}-en_GB.ts" || exit 1 )
done
if  [ "$1" == "ts" ]; then
  printf "Unfinished strings found:\n"
  grep "unfinished" -c translations/*ts
fi
