import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
  id: page

  // map dropdown selection to ints
  //TODO: is there a more native type for this?
  readonly property var inputMode: QtObject {
    property int sliders: 0
    property int text: 1
    property int swapper: 2
    property int generators: 3
    property int jolla: 4
    property int defaultValue: this.sliders
  }

  Component.onCompleted: { initColors() }
  onStatusChanged: {
    if ( status === PageStatus.Active ) { pageStack.pushAttached(Qt.resolvedUrl("Saver.qml")) }
  }

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: flw.height
    PageHeader { id: head ; title: qsTr("Adjust Theme Colors") }
    Flow {
      id: flw
      width: parent.width - Theme.horizontalPageMargin
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: head.bottom
      spacing: 0
      //spacing: Theme.paddingLarge
      // cool, but too noisy/jumpy
      //move: Transition { NumberAnimation { properties: "x,y"; duration: 240; easing.type: Easing.OutQuad } }
      add:      Transition { FadeAnimation { duration: 1200 } }
      move:     Transition { FadeAnimation { duration: 1200 } }
      populate: Transition { FadeAnimation { duration: 1200 } }
      //PageHeader { id: head ; title: qsTr("Adjust Theme Colors") }
      Column {
        id: srcol
        width: (isLandscape) ? ( parent.width / 2)  - Theme.horizontalPageMargin : parent.width
        CollapsingHeader {
          text: qsTr("Showroom")
          target: showroom
          BackgroundItem { anchors.fill: parent; onClicked: { showroom.visible = ! showroom.visible } }
        }
        Column {
          id: showroom
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          spacing: Theme.paddingMedium
          //add:      Transition { FadeAnimation { duration: 1200 } }
          //move:     Transition { FadeAnimation { duration: 1200 } }
          //populate: Transition { FadeAnimation { duration: 1200 } }
          onVisibleChanged: {
            sr1.visible = visible;
            sr2.visible = visible;
          }
          ShowRoomMini {
            visible: !sr1.visible && !sr2.visible
            BackgroundItem { anchors.fill: parent; onClicked: { sr1.visible = ! sr1.visible; sr2.visible = ! sr2.visible } }
          }
          ShowRoom {
            id: sr1
            mode: showRoomMode.text
            BackgroundItem { anchors.fill: parent; onClicked: { parent.visible = ! parent.visible } }
          }
          ShowRoom {
            id: sr2
            mode: showRoomMode.ui
            BackgroundItem { anchors.fill: parent; onClicked: { parent.visible = ! parent.visible } }
          }
        }
      }
      Column {
        id: labcol
        width: (isLandscape) ? ( parent.width / 2)  - Theme.horizontalPageMargin : parent.width
        //width: isLandscape ? parent.width / 2 : parent.width
        CollapsingHeader {
            width: parent.width
            text: qsTr("Laboratory")
            target: editcol
            BackgroundItem { anchors.fill: parent;
              onClicked: {
                      modeSelector.visible = ! modeSelector.visible;
                      editcol.visible = ! editcol.visible;
              }
            }
        }
        ComboBox {
          id: modeSelector
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          label: qsTr("Input Mode" + ":")
          description: qsTr("Tap to switch")
          currentIndex: inputMode.defaultValue
          menu: ContextMenu {
            // TODO: throws errors, but does work...
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            MenuItem { text: qsTr("Sliders") }
            MenuItem { text: qsTr("Text") }
            MenuItem { text: qsTr("Swapper/Copier") }
            MenuItem { text: qsTr("Generators") }
            MenuItem { text: qsTr("Jolla Original") }
          }
        }
        Column {
          id: editcol
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          ColorSliders {
              visible: modeSelector.currentIndex === inputMode.sliders
              opacity: visible ? 1.0 : 0
              Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          ColorTextInputs {
              visible: modeSelector.currentIndex === inputMode.text
              opacity: visible ? 1.0 : 0
              Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          ColorSwapper {
              visible: modeSelector.currentIndex === inputMode.swapper
              opacity: visible ? 1.0 : 0
              Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          ColorSwapper {
              visible: modeSelector.currentIndex === inputMode.swapper
              opacity: visible ? 1.0 : 0
              Behavior on opacity { FadeAnimation { duration: 800 } }
              copy: true
          }
          ColorGenerators {
              visible: modeSelector.currentIndex === inputMode.generators
              opacity: visible ? 1.0 : 0
              Behavior on opacity { FadeAnimation { duration: 800 } }
              GeneratorRemorse { id: generatorRemorse }
          }
          CollaSlider {
              visible: modeSelector.currentIndex === inputMode.jolla
              opacity: visible ? 1.0 : 0
              Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          // spacer to pullupmenu:
          SilicaItem {
            width: parent.width
            height: Theme.itemSizeLarge
          }
        }
      }
    }
    PullDownMenu {
        MenuItem { text: qsTr("Help");
                   onClicked: { pageStack.push(Qt.resolvedUrl("HelpPage.qml")) }
                 }
        MenuItem { text: qsTr("Open Ambience Settings");
                   onClicked: { settings.open() }
                 }
        MenuItem { text: qsTr("Apply Colors to System");
                   onClicked: { applyRemorse.execute(qsTr("Applying") + "…", function () { applyThemeColors() } ) }
                 }
        MenuItem { text: qsTr("Reload Colors from current Theme");
                   onClicked: { reloadThemeColors() }
                 }
        MenuItem { text: qsTr("Reload Colors from System Config");
                   onClicked: { colorsInitialized = false; initColors() }
                 }
    }
    PushUpMenu {
      quickSelect: false
        MenuLabel { text: qsTr("Experimental or dangerous actions") }
        MenuItem  { text: qsTr("Export to Ambience package");
                    onClicked: { pageStack.push(Qt.resolvedUrl("SaveAmbience.qml")) }
                  }
        MenuItem  { text: qsTr("Edit Transparency");
                    onClicked: { pageStack.push(Qt.resolvedUrl("TransparencyEditor.qml")) }
                  }
        MenuItem  { text: qsTr("Save Theme to current Ambience") + " " + qsTr("(not implemented)");
                    onClicked: { applyRemorse.execute( qsTr("Saving") + "…", function () { saveAmbience() } ) }
                    enabled: false;
                  }
        MenuItem  { text: qsTr("Reset all values and restart");
                    onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); restartLipstick(); } ) }
                  }
        MenuItem  { text: qsTr("Reset nonstandard values");
                    onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); } ) }
                  }
        MenuItem  { text: qsTr("Restart Lipstick");
                    onClicked: { applyRemorse.execute( qsTr("Restarting") + "…", function () { restartLipstick() } ) }
                  }
    }

    RemorsePopup {
      id: applyRemorse
    }

    VerticalScrollDecorator {}
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
