import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
  id: helppage

  allowedOrientations: (devicemodel === 'planetgemini') ? Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    contentHeight: col.height + Theme.itemSizeLarge
    anchors.fill: parent
    VerticalScrollDecorator {}
    Column {
        id: col
        width: parent.width - Theme.horizontalPageMargin
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: Theme.paddingLarge
        PageHeader { id: head ; title: qsTr("How to Use") }
        Separator {
          anchors.horizontalCenter: parent.horizontalCenter
          horizontalAlignment : Qt.AlignHCenter
          width: parent.width
          color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint)
        }
        SectionHeader {text: qsTr("General") }
        HelpLabel {
          text: qsTr(
'This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.<br />
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.<br/>
<br/>
We are working on overcoming some of these limitations.'
)
        }
        SectionHeader {text: qsTr("The Showroom") }
        HelpLabel {
          text: qsTr(
'The top area on the first page (\"Showroom\") shows the colors that are selected currently using various UI elements.<br />Here you can preview your creation.<br />Tapping on either area will hide it. To unhide, tap the title header.'
)
        }
        SectionHeader {text: qsTr("The Laboratory") }
        HelpLabel {
          text: qsTr(
'In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.<br />
Check what your theme will look like in the Showroom display.<br />
<br />
When you\'re done, use the PullDown Menu to apply the colors to the curent session.'
)
        }
        SectionHeader {text: qsTr("The Cupboard") }
        HelpLabel {
          text: qsTr(
'This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.<br />
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)'
)
        }
        SectionHeader {text: qsTr("Tips and Caveats") }
        HelpLabel {
          text: qsTr(
'Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.<br />
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
<br />
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.<br />
<br />
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.<br />
It\'s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.<br />
<br />
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:<br />
<pre>dconf dump /desktop/jolla/theme/color/</pre>
<pre>dconf reset /desktop/jolla/theme/color/highlightBackground</pre>
and repeat for all the other colors stored there. <br />
Changing the Ambience from the System Settings may also help.'
)
        }
        SectionHeader {text: qsTr("About") }
        DetailItem { label: qsTr("Version:");      value: AppInfo.versionstring }
        DetailItem { label: qsTr("Copyright:");    value: AppInfo.copyright;                                   BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.email) } }
        DetailItem { label: qsTr("License:");      value: AppInfo.license + " (" + AppInfo.licenseurl + ")";   BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.licenseurl) } }
        DetailItem { label: qsTr("Source Code:");  value: AppInfo.source;                                      BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.source) } }
        SectionHeader {text: qsTr("Translations:"); font.pixelSize: Theme.fontSizeTiny }
        Repeater {
          model: AppInfo.translators
          // TODO: how to get localized name of langid:
          //DetailItem { label: Qt.locale(langid).name + "/" + Qt.locale(langid).nativeLanguageName ;  value: persons;                          BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.transurl) } }
          DetailItem { label: qsTr(langname, "language name, translated") + "/" + Qt.locale(langid).nativeLanguageName ;  value: persons;                          BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.transurl) } }

        }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
