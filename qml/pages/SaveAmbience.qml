import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0
import Nemo.Configuration 1.0
//import Sailfish.Pickers 1.0
import "../components"

Page {
  id: page

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  readonly property string defaultPath: StandardPaths.documents
  property string exportPath : defaultPath
  property string cleanedUserName
  property string contentid
    /*
     * for the rpm builder to pick up
     */
    ConfigurationGroup {
      id: rpmparam
      path: "/org/nephros/openrepos-themecolor/rpmparam"
      synchronous: false
      property string ambname
      property string imgname
      property string ambfilename
     }


//  Component {
//    id: folderPicker
//    FolderPickerDialog {
//        title: qsTr("Export to")
//        onAccepted: userPath = selectedPath
//        onRejected: userPath = defaultPath
//    }
//  }
//
//  Component {
//    id: folderPicker
//    FolderPickerPage {
//        dialogTitle: qsTr("Export to")
//        onSelectedPathChanged: page.directoryPath = selectedPath
//    }
//  }

  Notification {
    id: popup
    previewSummary: "File exported."
    previewBody: "File has been placed into" + " " + exportPath
    appName: AppInfo.displayName
    category: "transfer.complete"
    isTransient: true
    icon: "image://theme/icon-lock-installed"
  }

  SilicaFlickable {
    id: flick
    contentHeight: col.height
    anchors.fill: parent
    Column {
        id: col
        width: parent.width - Theme.horizontalPageMargin
        spacing: Theme.paddingSmall
        anchors.horizontalCenter: parent.horizontalCenter
        PageHeader { title: qsTr("Export Functions") }
        Label {
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          font.pixelSize: Theme.fontSizeSmall
          horizontalAlignment: Text.AlignJustify
          wrapMode: Text.WordWrap
          //text: qsTr("Here you can export your creation to a .ambience (json) file.<br />
          //            This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
          //           ")
          text: qsTr("Here you can export your creation to a .ambience (json) file.<br />
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     ")
        }
        SectionHeader { text: qsTr("Export to File") }
        TextField {
            id: username
            anchors.horizontalCenter: parent.horizontalCenter
            width: Math.max(implicitWidth, parent.width - Theme.itemSizeMedium)
            label: qsTr("Ambience Name")
            labelVisible: true
            placeholderText: qsTr("A cool Ambience Name")
            font.pixelSize: Theme.fontSizeLarge
            focus: true
            //validator: RegExpValidator { regExp: /^[-_\.\w\s]+$/ }
            EnterKey.enabled: text.length > 0
            EnterKey.onClicked: {
                cleanedUserName = text.replace(/[^\.\-_a-zA-Z0-9]/g, "_");
                contentid = Qt.md5(Date.now() + text)
                focus = false
            }
        }
//        ValueButton {
//            id: userpath
//            anchors.horizontalCenter: parent.horizontalCenter
//            label: qsTr("Path")
//            labelVisible: true
//            value: userPath ? userPath : defaultPath
//            onClicked: { pageStack.push(folderPicker) }
//        }
        ValueButton {
          anchors.horizontalCenter: parent.horizontalCenter
          width: Math.max(implicitWidth, username.width)
          description: qsTr("Click to export")
          label: exportPath + "/"
          value: cleanedUserName ? cleanedUserName + "_" + contentid.substr(0,8) + ".ambience" : ""
          enabled: cleanedUserName.length > 0
          onClicked: {
            /*
             * prepare data
             */
            function colToHexStr(color, alpha) {
              console.log("got:" + color + ", alpha: " + alpha );
              var str = color.substr(1,6);
              var r = Number(parseInt( color.r , 10)).toString(16);
              var g = Number(parseInt( color.g , 10)).toString(16);
              var b = Number(parseInt( color.b , 10)).toString(16);
              // stupid
              var a = (alpha == 1 ) ? "ff" : Number(parseInt( alpha , 10)).toString(16);
              a = (alpha == 0 ) ? "00" : a;
              //var ret = "#" + a + r + g + b;
              var ret = "#" + a + str;
              console.log("returning" + ret);
              return ret;
            }
            var data = JSON.parse('{}');
            // ambience expects full rgba, 8 character color strings, but Theme.rgba squishes alpha if 1
            data.primaryColor             = colToHexStr(MyPalette.primaryColor.toString(),1.0);
            data.secondaryColor           = MyPalette.secondaryColor.toString();
            data.highlightColor           = colToHexStr(MyPalette.highlightColor.toString(),1.0);
            data.secondaryHighlightColor  = MyPalette.secondaryHighlightColor.toString();
            //data.highlightBackgroundColor = MyPalette.highlightBackgroundColor.toString();
            //data.highlightDimmerColor     = MyPalette.highlightDimmerColor.toString();
            //data.overlayBackgroundColor   = MyPalette.overlayBackgroundColor.toString();
            //data.backgroundGlowColor      = MyPalette.backgroundGlowColor.toString();
            //data._wallpaperOverlayColor   = MyPalette._wallpaperOverlayColor.toString();
            //data._coverOverlayColor       = MyPalette._coverOverlayColor.toString();

            data.colorScheme           = (MyPalette.colorScheme == Theme.LightOnDark) ? "lightondark" : "darkonlight" ;
            data.displayName           = username.text;
            data.version               = 3;
            data.wallpaper             = imagepath.value;
            //data.ringerVolume          = -1;
            //data.translationCatalog    = username.text;
            //data.favorite              = "true";

            //data.contentId           = contentid;
            console.debug("constructed data:" + JSON.stringify(data) );

            /*
             * determine output file path and name
             */
            var filename = cleanedUserName + "_" + contentid.substr(0,8) + ".ambience";
            var filePath = "/" + exportPath + "/" + filename;
            var fileUrl = "file://" + filePath;

            /*
             * contruct upload request
             */
            var r = new XMLHttpRequest()
            r.open('PUT', fileUrl);
            r.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            r.setRequestHeader('Content-length', JSON.stringify(data).length);
            r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            r.send(JSON.stringify(data));

            r.onreadystatechange = function(event) {
              if (r.readyState == XMLHttpRequest.DONE) {
                  console.debug("request done.");
              }
            }
            console.info("Wrote to:" + fileUrl + " from " + Qt.application.name );
            console.debug("data:" + JSON.stringify(data) );
            popup.publish()

            rpmparam.ambname = cleanedUserName;
            rpmparam.imgname = imagepath.value;
            rpmparam.ambfilename = filePath;
            rpmparam.sync();
            //console.debug("opening: " + "file://" + exportPath) 
            //Qt.openUrlExternally("file://" + exportPath)
            help.visible = true;
          }
        }
        Label {
          id: help
          visible: false
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          font.pixelSize: Theme.fontSizeSmall
          horizontalAlignment: Text.AlignJustify
          wrapMode: Text.WordWrap
          text: qsTr("Launch the" + " " + qsTr(AppInfo.builderDisplayName) + " " + "to generate and install the ambience RPM.<br />"
                    + "You can edit the file before if you like, and it will be picked up by the builder as long as the file name stays the same."
                    + " " + qsTr(AppInfo.builderDisplayName) + " " + "always picks up the latest exported file and does not remember previous ones."
                    )
        }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
