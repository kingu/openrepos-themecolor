import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "../components"
import "../components/saver"
import "savefunctions.js" as Saver

Page {
  id: saver

  property alias slots: storeslots.value
  property string ambuuid
  property string ambname: qsTr("anonymous")

  ConfigurationGroup {
      id: store
      path: "/org/nephros/openrepos-themecolor/storage/" + ambuuid
  }
  ConfigurationValue {
      id: storeslots
      key: "/org/nephros/openrepos-themecolor/storage/slots"
      defaultValue: 4
  }

  ConfigurationValue {
      id: amburl
      key: "/desktop/jolla/theme/active_ambience"
  }
  ConfigurationValue {
    id: ambthumb
    key: "/desktop/jolla/background/portrait/app_picture_filename"
  }

  function loadTheme(i) {
      MyPalette.primaryColor             = store.value("store" + i + "_p")
      MyPalette.secondaryColor           = store.value("store" + i + "_s")
      MyPalette.highlightColor           = store.value("store" + i + "_h")
      MyPalette.secondaryHighlightColor  = store.value("store" + i + "_sh")
      MyPalette.highlightBackgroundColor = store.value("store" + i + "_hbg")
      MyPalette.backgroundGlowColor      = store.value("store" + i + "_bgg")
      MyPalette.highlightDimmerColor     = store.value("store" + i + "_hdc")
      //  we can just pop here because we are an Attached page of an Attached
      //  page, so the top of the stack is our previous page really...
      pageStack.pop()
  }
  function saveTheme(i) {
      store.setValue("store" + i + "_p",    MyPalette.primaryColor);
      store.setValue("store" + i + "_s",    MyPalette.secondaryColor);
      store.setValue("store" + i + "_h",    MyPalette.highlightColor);
      store.setValue("store" + i + "_sh",   MyPalette.secondaryHighlightColor);
      store.setValue("store" + i + "_hbg",  MyPalette.highlightBackgroundColor);
      store.setValue("store" + i + "_bgg",  MyPalette.backgroundGlowColor);
      store.setValue("store" + i + "_hdc",  MyPalette.highlightDimmerColor);
      store.sync();
  }
  function setThemeProp(i, prop, val) {
      if ( typeof(val) === "undefined" ) {
        //console.warn("trying to save uninitialized value, saving gray instead");
        //store.setValue("store" + i + "_" + prop, "'dimgray'");
        console.warn("trying to save uninitialized value");
        return;
      }
      store.setValue("store" + i + "_" + prop, val);
  }
  function getThemeProp(i, prop) {
      var c =  store.value("store" + i + "_" + prop);
      if ( typeof(c) === "undefined") {
         console.debug("returning gray instead of uninitialized value for " + prop);
         return "dimgray";
       } else if (c === "dimgray") {
         console.debug("correcting saved dimgray value for " + prop);
         store.setValue("store" + i + "_" + prop, '');
         return "dimgray";
       } else {
        return c;
       }
  }
  function getAmbienceInfo(url) {
    var xhr = new XMLHttpRequest();

    xhr.open("GET",url);
    xhr.onreadystatechange = function() {
      if ( xhr.readyState == xhr.DONE) {
          var j = JSON.parse(xhr.response);
          var name = j.displayName;
          ambname = name
      }
    }
    xhr.send();
  }
  function getAmbienceUid() {
      // can be either local uid, or a .ambience path
      //var r = /.*\/([\S]{34})\.[^.]*$/gi;
      var r = /.*\/([\S]+)\.[^.]*$/gi;
      var uid = amburl.value.replace(r, "$1");
      return uid
  }

  Component.onCompleted: {
    ambuuid = getAmbienceUid()
    if ( amburl.value.match(/\.ambience$/) ) {
      getAmbienceInfo("file://" + amburl.value)
    }
  }

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: parent.height
    VerticalScrollDecorator {}
    PageHeader { id: head ; title: ambname + " " + qsTr("Ambience Cupboard")
      Image {
        id: thumb
        anchors{
          verticalCenter: parent.verticalCenter
          left: parent.left
          rightMargin: Theme.paddingLarge * 2
          leftMargin: Theme.paddingLarge * 2
        }
        source: ambthumb.value
        sourceSize.height: Theme.iconSizeMedium
        fillMode: Image.PreserveAspectCrop
        clip: true
        cache: false
        opacity: Theme.opacityHigh
      }
    }
    Separator {
      id: sep
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.top: head.bottom
       width: parent.width
       //height: 2
       color: Theme.secondaryColor
    }
    SilicaListView {
      id: view
      anchors.top: head.bottom
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: flick.height - (head.height + sep.height )
      clip: true
      currentIndex: -1
      model: slots
      delegate: SaveSlot {
        idx: index
        ambience: true;
        width: ListView.view.width
      }
      pullDownMenu: pull
    }
    RemorsePopup { id: remorse }
    PullDownMenu {
      id: pull
      MenuItem { text: qsTr("Clean out this cupboard"); onClicked: remorse.execute( ( qsTr("Spring Clean") + "…" ), function() { store.clear(); pageStack.navigateBack() } ,6000) }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
