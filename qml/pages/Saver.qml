import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "../components"
import "../components/saver"
import "savefunctions.js" as Saver

Page {
  id: saver

  property alias slots: store.slots
  property alias lootboxtapped: store.lootboxtapped

  ConfigurationGroup {
      id: store
      path: "/org/nephros/openrepos-themecolor/storage"
      property int slots: 4
      property int lootboxtapped: 0
  }

  function loadTheme(i) {
      MyPalette.primaryColor             = store.value("store" + i + "_p")
      MyPalette.secondaryColor           = store.value("store" + i + "_s")
      MyPalette.highlightColor           = store.value("store" + i + "_h")
      MyPalette.secondaryHighlightColor  = store.value("store" + i + "_sh")
      MyPalette.highlightBackgroundColor = store.value("store" + i + "_hbg")
      MyPalette.backgroundGlowColor      = store.value("store" + i + "_bgg")
      MyPalette.highlightDimmerColor     = store.value("store" + i + "_hdc")
      pageStack.navigateBack()
  }
  function saveTheme(i) {
      store.setValue("store" + i + "_p",    MyPalette.primaryColor);
      store.setValue("store" + i + "_s",    MyPalette.secondaryColor);
      store.setValue("store" + i + "_h",    MyPalette.highlightColor);
      store.setValue("store" + i + "_sh",   MyPalette.secondaryHighlightColor);
      store.setValue("store" + i + "_hbg",  MyPalette.highlightBackgroundColor);
      store.setValue("store" + i + "_bgg",  MyPalette.backgroundGlowColor);
      store.setValue("store" + i + "_hdc",  MyPalette.highlightDimmerColor);
      store.sync();
  }
  function setThemeProp(i, prop, val) {
      if ( typeof(val) === "undefined" ) {
        //console.warn("trying to save uninitialized value, saving gray instead");
        //store.setValue("store" + i + "_" + prop, "'dimgray'");
        console.warn("trying to save uninitialized value");
        return;
      }
      store.setValue("store" + i + "_" + prop, val);
  }
  function getThemeProp(i, prop) {
      var c =  store.value("store" + i + "_" + prop);
      if ( typeof(c) === "undefined") {
         console.debug("returning gray instead of uninitialized value for " + prop);
         return "dimgray";
       } else if (c === "dimgray") {
         console.debug("correcting saved dimgray value for " + prop);
         store.setValue("store" + i + "_" + prop, '');
         return "dimgray";
       } else {
        return c;
       }
  }
  Component.onCompleted: {
    if ( lootboxtapped >= 3 ) {
      slots += 2;
      lootboxtapped = 0;
    }
  }

  onStatusChanged: {
    // SaverPlus depends on being Attached! so it can pop()
    if ( status === PageStatus.Active ) { pageStack.pushAttached( Qt.resolvedUrl("SaverPlus.qml")) }
  }

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: parent.height
    VerticalScrollDecorator {}
    PageHeader { id: head ; title: qsTr("Global Cupboard") }
    Separator {
      id: sep
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.top: head.bottom
       width: parent.width
       //height: 2
       color: Theme.secondaryColor
    }
    SilicaListView {
      id: view
      anchors.top: head.bottom
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: flick.height - (head.height + sep.height )
      clip: true
      currentIndex: -1
      model: slots
      delegate: SaveSlot {
        idx: index
        width: ListView.view.width
      }
      pullDownMenu: pull
      footer: LootBoxItem {
        id: loot
        numtapped: lootboxtapped
        onNumtappedChanged: { lootboxtapped = numtapped; }
      }
    }
    RemorsePopup { id: remorse }
    PullDownMenu {
      id: pull
      MenuItem { text: qsTr("Clean out this cupboard"); onClicked: remorse.execute( ( qsTr("Spring Clean") + "…"),
        function() {
          var tmp = slots;
          store.clear();
          store.setValue("slots", tmp);
          pageStack.navigateBack();
        } ,6000) }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
