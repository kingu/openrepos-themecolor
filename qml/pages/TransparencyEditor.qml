import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "../components"

Dialog {
  id: transpage

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  function printValues(text) {
    console.debug( text );
    console.debug("\tHighlight Background Opacity\t" + Theme.highlightBackgroundOpacity);
    //console.debug("\tFaint Opacity\t" + MyPalette.opacityFaint);
    //console.debug("\tLow Opacity\t" + MyPalette.opacityLow);
    //console.debug("\tHigh Opacity\t" + MyPalette.opacityHigh);
    //console.debug("\tOverlay Opacity\t" + MyPalette.opacityOverlay);
    console.debug("\tsecondaryColor\t" + MyPalette.secondaryColor);
    console.debug("\tsecondaryHighlightColor\t" + MyPalette.secondaryHighlightColor);
  }
  onOpened: {
    printValues("Colors on Open: ")
  }
  //onDone: {
  //  printValues("Colors on Done: ")
  //}
  onAccepted: {
      printValues("Colors on Accept: ")
  }
  onRejected: {
      MyPalette.secondaryColor            = Theme.rgba(MyPalette.secondaryHighlightColor, MyPalette.s_alpha)
      MyPalette.secondaryHighlightColor   = Theme.rgba(MyPalette.secondaryHighlightColor, MyPalette.shl_alpha)
      printValues("Colors on Cancel: ") 
  }

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: col.height + head.height
    VerticalScrollDecorator {}
    DialogHeader { id: head ; title: qsTr("Edit Transparency") }
    Column {
      id: col
      width: parent.width - Theme.paddingLarge
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: head.bottom
      spacing: Theme.paddingLarge
      HelpLabel { text: qsTr("This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.") }
      /*
       * Opacity values
       */
      //SectionHeader { text: qsTr("General" + " " + "Opacity" +  " " + "Values") }
      //HelpLabel { text: qsTr("Four opacity types are saved in the theme: Faint, Medium, Low, and Overlay. The slider below sets general Opacity, the type values are computed from your selection."); font.pixelSize: Theme.fontSizeExtraSmall }
      //Row {
      //  anchors.horizontalCenter: parent.horizontalCenter
      //  spacing: Theme.paddingLarge
      //  Repeater {
      //    height: Theme.iconSizeLarge
      //    width: parent.width
      //    model: 4
      //    delegate: Rectangle {
      //        property double factor: ((index + 1)/4) // see also computation in MyPalette
      //        Text { anchors.centerIn: parent; color: MyPalette.secondaryColor; text: Number(MyPalette.genericAlpha * parent.factor ).toFixed(2) }
      //        color: Theme.rgba(MyPalette.highlightBackgroundColor , MyPalette.genericAlpha * factor )
      //        height: Theme.iconSizeLarge
      //        width: Theme.iconSizeLarge
      //        radius: Theme.paddingSmall
      //    }
      //  }
      //}
      //ColorSliderAlpha { id: genslider; target: MyPalette.highlightBackgroundColor; alpha: MyPalette.genericAlpha; editbg: true; noicon: true; onOutChanged: {MyPalette.genericAlpha = out} }
      SectionHeader { text: qsTr("Highlight Background" + " " + "Opacity") }
      HelpLabel { text: qsTr("This is used e.g. for Pulley Menu background."); font.pixelSize: Theme.fontSizeExtraSmall }
      ColorSliderAlpha { target: MyPalette.highlightBackgroundColor; alpha: MyPalette.highlightBackgroundOpacity; editbg: true; onOutChanged: {MyPalette.highlightBackgroundOpacity = out} }
      /*
       * Color Alpha channels
       */
      SectionHeader {    text: qsTr("Color Alpha Channel") }
      HelpLabel {        text: qsTr("Here you can edit Alpha channels for colors that have one."); font.pixelSize: Theme.fontSizeExtraSmall}
      Label {            text: qsTr("Secondary Color"); color: Theme.secondaryColor; font.pixelSize: Theme.fontSizeExtraSmall }
      ColorSliderAlpha { target: MyPalette.secondaryColor; alpha: MyPalette.secondaryColor.a; onOutChanged: { MyPalette.secondaryColor = Theme.rgba(MyPalette.secondaryColor, out) } }
      Label {            text: qsTr("Secondary Highlight Color"); color: Theme.secondaryColor; font.pixelSize: Theme.fontSizeExtraSmall }
      ColorSliderAlpha { target: MyPalette.secondaryHighlightColor; alpha: MyPalette.secondaryHighlightColor.a; onOutChanged: { MyPalette.secondaryHighlightColor = Theme.rgba(MyPalette.secondaryHighlightColor, out) } }

     }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
