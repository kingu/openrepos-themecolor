import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

SilicaItem {
    id: minishowroom
    highlighted: false
    height: showcol.height + header.height
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter

    SectionHeader {
      id: header
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: implicitHeight
      font.pixelSize: Theme.fontSizeTiny
      text: qsTr("Mini", "small showroom") + "-" + qsTr("Showroom");
    }

    Column {
        id: showcol
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: header.bottom
        spacing: 1
        Row {
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          spacing: 0
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.primaryColor }
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.secondaryColor }
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.highlightColor }
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.secondaryHighlightColor }
        }
        Row {
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          spacing: 0
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.overlayBackgroundColor }
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.highlightBackgroundColor }
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette.highlightDimmerColor }
          Rectangle { height: Theme.paddingSmall; width: showcol.width / 4 ;  color: MyPalette._coverOverlayColor }
        }
    }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
