
import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

SilicaItem {
  height: header.height
  width: parent.width
  property var target
  property alias text: header.text
  Icon {
    id: icon
    anchors.verticalCenter: sep.verticalCenter
    anchors.right: parent.right
    height: Theme.iconSizeSmall
    width: Theme.iconSizeSmall
    source: "image://theme/icon-s-unfocused-down?" + parent.color
    rotation: target.visible ? 360 : 90
    Behavior on rotation { PropertyAnimation { } }
  }
  SectionHeader {
    id: header
    width: parent.width
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    font.pixelSize: Theme.fontSizeLarge
    color: ( target.visible ) ? Theme.highlightColor : Theme.secondaryColor
    Behavior on color { ColorAnimation { } }
  }
  Separator {
    id: sep
    anchors.top: header.bottom
    width: parent.width - ( icon.width )
    height: 2
    horizontalAlignment: target.visible ? Qt.AlignRight : Qt.AlignHCenter
    color: Theme.highlightColor
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
