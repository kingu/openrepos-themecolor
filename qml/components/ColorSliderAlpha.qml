import QtQuick 2.6
import Sailfish.Silica 1.0
import "."

Item {
    property color target
    property double alpha: target.a
    property bool editbg: false
    property bool noicon: false
    property double out

    width: parent.width
    height: Math.max(btn.height, tslider.height)
    anchors.horizontalCenter: parent.horizontalCenter

    Component.onCompleted: {
      if (typeof(alpha) === "undefined" ) {alpha = target.a};
      tslider.value = alpha;
    }

    IconButton {
      id: btn
      visible: !noicon
      anchors.topMargin: Theme.paddingSmall;
      icon.sourceSize.height: Theme.iconSizeMedium;
      icon.source: parent.editbg ? "image://theme/icon-m-ambience?" + MyPalette.secondaryColor : "image://theme/icon-m-ambience?" + Theme.rgba(parent.target, tslider.value)

      TouchBlocker { anchors.fill: parent }
      Rectangle {
        color: (editbg) ? Theme.rgba(target, tslider.value) : Theme.rgba(MyPalette.highlightBackgroundColor , Theme.highlightBackgroundOpacity)
        anchors.fill: parent
        radius: Theme.paddingSmall
      }
    }
    Slider {
      id: tslider
      width: parent.width - btn.width
      handleVisible: true
      highlighted: false
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.verticalCenter: btn.verticalCenter
      minimumValue: 0; maximumValue: 1.0; stepSize: 2/256
      //minimumValue: 0; maximumValue: 1.0; stepSize: 1/256
      //valueText: "#" + Number(parseInt( value * 255 , 10)).toString(16);
      //valueText: { Number(value).toFixed(2) }
      valueText: Number(Math.floor(value * 100)) + "%"
      // dont let the user slide into invisibility
      color: (value > 0.1) ? Theme.rgba( Theme.primaryColor, value ) : Theme.rgba( Theme.primaryColor, 0.1 )
      onValueChanged: {
        var tmp = value;
        tslider.enabled = false;
        target = Theme.rgba( target, tmp );
        tslider.enabled = true;
      }
      onReleased: {
        parent.out = value
      }
     }
}
// vim: expandtab ts=4 st=4 filetype=javascript
