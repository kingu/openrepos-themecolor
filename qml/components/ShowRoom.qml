import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "showroom"

SilicaItem {
    highlighted: false
    height: showcol.height + header.height
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter

    readonly property var showRoomMode: QtObject {
      property int text: 0
      property int ui: 1
      property int defaultValue: this.text
    }

    property int mode: showRoomMode.defaultValue

    SectionHeader {
      id: header
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: implicitHeight
      text: {
        switch (mode) {
          case showRoomMode.text: return qsTr("Text Elements");
          case showRoomMode.ui:   return qsTr("UI Elements");
        }
      }
    }

   ShowRoomBG {
        anchors.top: showcol.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: showcol.width
        height: showcol.height
    }
    Column {
        id: showcol
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: header.bottom
        spacing: Theme.paddingMedium
        ShowRoomText { visible: mode == showRoomMode.text }
        ShowRoomUI   { visible: mode == showRoomMode.ui }
    }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
