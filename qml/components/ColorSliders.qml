import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "controls"

Item {
  anchors.horizontalCenter: parent.horizontalCenter
  //width: page.isLandscape ? parent.width - Theme.itemSizeLarge * 3 : parent.width - Theme.horizontalPageMargin * 2
  width: parent.width
  height: col.height

  Column {
    id: col
    width: parent.width
    SectionHeader { text: qsTr("Primary Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.primaryColor = outcolor
      onVisibleChanged: { incolor = MyPalette.primaryColor }
    }
    SectionHeader { text: qsTr("Secondary Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.secondaryColor = outcolor
      onVisibleChanged: { incolor = MyPalette.secondaryColor }
    }
    SectionHeader { text: qsTr("Highlight Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.highlightColor = outcolor
      onVisibleChanged: { incolor = MyPalette.highlightColor }
    }
    SectionHeader { text: qsTr("Secondary Highlight Color") ; color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.secondaryHighlightColor = outcolor
      onVisibleChanged: { incolor = MyPalette.secondaryHighlightColor }
    }
    SectionHeader { text: qsTr("Highlight Background Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.highlightBackgroundColor = outcolor
      onVisibleChanged: { incolor = MyPalette.highlightBackgroundColor }
    }
    SectionHeader { text: qsTr("Background Glow Color"); color: Theme.primaryColor}
    GlowSlider { }
   }
}

// vim: expandtab ts=4 st=4 filetype=javascript
