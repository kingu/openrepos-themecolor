function computeColors(MyPalette) {
  if (Theme.colorScheme == Theme.LightOnDark) { MyPalette.primaryColor   = "#FFFFFFFF" } else { MyPalette.primaryColor   = "#FF000000"};
  if (Theme.colorScheme == Theme.LightOnDark) { MyPalette.secondaryColor = "#B0FFFFFF" } else { MyPalette.secondaryColor = "#B0000000" };
  // TODO: what to choose here exactly:
  MyPalette.highlightColor              = Theme.highlightFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
  MyPalette.secondaryHighlightColor     = Theme.secondaryHighlightFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
  MyPalette.highlightBackgroundColor    = Theme.highlightBackgroundFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
  MyPalette.highlightDimmerColor        = Theme.highlightDimmerFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
  MyPalette.backgroundGlowColor         = Theme.backgroundGlowColor
}
function tintColors(MyPalette, tint) {
  MyPalette.primaryColor             = Qt.tint(MyPalette.primaryColor, tint)
  MyPalette.secondaryColor           = Qt.tint(MyPalette.secondaryColor, tint)
  MyPalette.highlightColor           = Qt.tint(MyPalette.highlightColor, tint)
  MyPalette.secondaryHighlightColor  = Qt.tint(MyPalette.secondaryHighlightColor, tint)
  MyPalette.highlightBackgroundColor = Qt.tint(MyPalette.highlightBackgroundColor, tint)
  MyPalette.highlightDimmerColor     = Qt.tint(MyPalette.highlightDimmerColor, tint)
  MyPalette.backgroundGlowColor      = Qt.tint(MyPalette.backgroundGlowColor, tint)
}
function darkenColors(MyPalette, factor) {
  MyPalette.primaryColor             = Qt.darker(MyPalette.primaryColor, factor)
  MyPalette.secondaryColor           = Qt.darker(MyPalette.secondaryColor, factor)
  MyPalette.highlightColor           = Qt.darker(MyPalette.highlightColor, factor)
  MyPalette.secondaryHighlightColor  = Qt.darker(MyPalette.secondaryHighlightColor, factor)
  MyPalette.highlightBackgroundColor = Qt.darker(MyPalette.highlightBackgroundColor, factor)
  MyPalette.highlightDimmerColor     = Qt.darker(MyPalette.highlightDimmerColor, factor)
  MyPalette.backgroundGlowColor      = Qt.darker(MyPalette.backgroundGlowColor, factor)
}
function lightenColors(MyPalette, factor) {
  MyPalette.primaryColor             = Qt.lighter(MyPalette.primaryColor, factor)
  MyPalette.secondaryColor           = Qt.lighter(MyPalette.secondaryColor, factor)
  MyPalette.highlightColor           = Qt.lighter(MyPalette.highlightColor, factor)
  MyPalette.secondaryHighlightColor  = Qt.lighter(MyPalette.secondaryHighlightColor, factor)
  MyPalette.highlightBackgroundColor = Qt.lighter(MyPalette.highlightBackgroundColor, factor)
  MyPalette.highlightDimmerColor     = Qt.lighter(MyPalette.highlightDimmerColor, factor)
  MyPalette.backgroundGlowColor      = Qt.lighter(MyPalette.backgroundGlowColor, factor)
}


/*
 * - D N E V N O Y   D O Z O R -
 * =============================
 */
function dayColors(MyPalette) {
  //MyPalette.colorScheme = Theme.DarkOnLight;
  //computeColors(MyPalette);
  var factor = 1.8;
  if (MyPalette.colorScheme == Theme.DarkOnLight) {
    MyPalette.primaryColor             = Qt.darker(MyPalette.primaryColor,   2*factor)
    MyPalette.secondaryColor           = Qt.darker(MyPalette.secondaryColor, 2*factor)
    MyPalette.highlightColor           = Qt.darker(MyPalette.highlightColor, factor);
    MyPalette.secondaryHighlightColor  = Qt.darker(MyPalette.secondaryHighlightColor, factor);
    MyPalette.highlightBackgroundColor = Qt.lighter(MyPalette.highlightBackgroundColor, factor);
    MyPalette.highlightDimmerColor     = Qt.lighter(MyPalette.highlightDimmerColor, factor);
  } else {
    MyPalette.primaryColor             = Qt.lighter(MyPalette.primaryColor,   2*factor)
    MyPalette.secondaryColor           = Qt.lighter(MyPalette.secondaryColor, 2*factor)
    MyPalette.highlightColor           = Qt.lighter(MyPalette.highlightColor, factor);
    MyPalette.secondaryHighlightColor  = Qt.lighter(MyPalette.secondaryHighlightColor, factor);
    MyPalette.highlightBackgroundColor = Qt.darker(MyPalette.highlightBackgroundColor, factor);
    MyPalette.highlightDimmerColor     = Qt.darker(MyPalette.highlightDimmerColor, factor);
  }
}
/*
 * - N O C H N O Y   D O Z O R -
 * =============================
 */
function nightColors(MyPalette) {
  //computeColors();
  var factor = 1.8; darkenColors(MyPalette, factor);
  // reduce blue by tinting yellow
  var yellower = "#10FFFF00"; tintColors(MyPalette, yellower);
  // reduce "brightness"
  var grayer = "#20b0b0b0"; tintColors(MyPalette, grayer);
  // add a little red
  var tint = "#20ff0000"; tintColors(MyPalette, tint);
}
/*
 * - S O L A R I Z E -
 * ===================
 */
function solarizeColors(MyPalette) {
  // from https://en.wikipedia.org/wiki/Solarized_(color_scheme)
  var brblack   =   "#002b36";
  var black     =   "#073642";
  var brgreen   =   "#586e75";
  var bryellow  =   "#657b83";
  var brblue    =   "#839496";
  var brcyan    =   "#93a1a1";
  var white     =   "#eee8d5";
  var brwhite   =   "#fdf6e3";
  var yellow    =   "#b58900";
  var brred     =   "#cb4b16";
  var red       =   "#dc322f";
  var magenta   =   "#d33682";
  var brmagenta =   "#6c71c4";
  var blue      =   "#268bd2";
  var cyan      =   "#2aa198";
  var green     =   "#859900";
  MyPalette.primaryColor             = Qt.tint(MyPalette.primaryColor, brgreen);
  MyPalette.secondaryColor           = Qt.tint(MyPalette.secondaryColor, bryellow);
  MyPalette.highlightColor           = Qt.tint(MyPalette.highlightColor, yellow);
  MyPalette.secondaryHighlightColor  = Qt.tint(MyPalette.secondaryHighlightColor, brred);
  MyPalette.highlightBackgroundColor = Qt.tint(MyPalette.highlightBackgroundColor, blue);
  MyPalette.highlightDimmerColor     = Qt.tint(MyPalette.highlightDimmerColor, black);
  MyPalette.backgroundGlowColor      = Qt.tint(MyPalette.backgroundGlowColor, cyan);
}

/*
 * - R A N D O M I Z E -
 * =====================
 */
function rndColors(MyPalette) {
  MyPalette.primaryColor             = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
  MyPalette.secondaryColor           = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.secondaryColor.a);
  MyPalette.highlightColor           = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.highlightColor.a);
  MyPalette.secondaryHighlightColor  = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.secondaryHighlightColor.a);
  MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  MyPalette.highlightDimmerColor     = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  MyPalette.backgroundGlowColor      = Qt.rgba(Math.random(), Math.random(), Math.random(), Theme.backgroundGlowColor.a);
}
function rndBrightOrDark(MyPalette) {
  MyPalette.primaryColor            = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
  MyPalette.secondaryColor          = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.secondaryColor.a);
  MyPalette.highlightColor          = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.highlightColor.a);
  MyPalette.secondaryHighlightColor = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.secondaryHighlightColor.a);
  MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  MyPalette.highlightDimmerColor    = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  MyPalette.backgroundGlowColor     = Qt.rgba(Math.random(), Math.random(), Math.random(), Theme.backgroundGlowColor.a);
  var factor = 1.2;
  if ( MyPalette.colorScheme == Theme.LightOnDark ) {
    lightenColors(MyPalette, factor);
  } else {
    darkenColors(MyPalette, factor);
  }
  //MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  //MyPalette.highlightDimmerColor = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
}
function rndGray(MyPalette) {
  var cmax = ( Theme.colorScheme == Theme.LightOnDark ) ? 1.0 : 0.4;
  var cmin = ( Theme.colorScheme == Theme.LightOnDark ) ? 0.6 : 0.0;
  var rp = Math.random() * (cmax - cmin) + cmin
  var rs = Math.random() * (cmax - cmin) + cmin
  var rh = Math.random() * (cmax - cmin) + cmin
  var rsh = Math.random() * (cmax - cmin) + cmin
  var rbgg = Math.random() * (cmax - cmin) + cmin
  MyPalette.primaryColor            = Qt.rgba(rp, rp, rp, MyPalette.primaryColor.a);
  MyPalette.secondaryColor          = Qt.rgba(rs, rs, rs, MyPalette.secondaryColor.a);
  MyPalette.highlightColor          = Qt.rgba(rh, rh, rh, MyPalette.highlightColor.a);
  MyPalette.secondaryHighlightColor = Qt.rgba(rsh, rsh, rsh, MyPalette.secondaryHighlightColor.a);
  MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  MyPalette.highlightDimmerColor     = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
  MyPalette.backgroundGlowColor      = Qt.rgba(rbgg, rbgg, rbgg, MyPalette.backgroundGlowColor.a);
}


/*
 * - C O L O R   B L I N D N E S S -
 * ===============================
 */

// from https://www.visualisingdata.com/2019/08/five-ways-to-design-for-red-green-colour-blindness/
// variation #1:  #db4325 #eda247 #e6e1bc #57c4ad #006164
// variation #2:  #b3589a #d091bb #f6f3e8 #e7f7d5 #bbd4a6 #9bbf85
// variation #3:  #ca0020 #f4a582 #f7f7f7 #92c5de #0571b0
//
// from https://colorbrewer2.org/#type=diverging&scheme=RdYlBu&n=5
//                #d7191c #fdae61 #ffffbf #abd9e9 #2c7bb6
//                #a6611a #dfc27d #f5f5f5 #80cdc1 #018571
function blindColors(MyPalette, idx){
  computeColors(MyPalette);
  var map = [
    ["#db4325", "#eda247", "#e6e1bc", "#57c4ad", "#006164"],
    ["#ca0020", "#f4a582", "#f7f7f7", "#92c5de", "#0571b0"],
    ["#d7191c", "#fdae61", "#ffffbf", "#abd9e9", "#2c7bb6"],
    ["#a6611a", "#dfc27d", "#f5f5f5", "#80cdc1", "#018571"]
  ];
  MyPalette.primaryColor             = Qt.tint(MyPalette.primaryColor             , map[idx][2]);
  MyPalette.secondaryColor           = Qt.tint(MyPalette.secondaryColor           , map[idx][1]);
  MyPalette.highlightColor           = Qt.tint(MyPalette.highlightColor           , map[idx][3]);
  MyPalette.secondaryHighlightColor  = Qt.tint(MyPalette.secondaryHighlightColor  , map[idx][0]);
  MyPalette.highlightBackgroundColor = Qt.tint(MyPalette.highlightBackgroundColor , map[idx][4]);
  //MyPalette.highlightDimmerColor     = Qt.tint(MyPalette.highlightDimmerColor, );
  //MyPalette.backgroundGlowColor      = Qt.tint(MyPalette.backgroundGlowColor, );
}

// vim: expandtab ts=4 st=4 filetype=javascript
