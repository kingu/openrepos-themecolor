import QtQuick 2.6
import Sailfish.Silica 1.0
import "functions.js" as Functions
import ".."

ButtonLayout {
    //width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    preferredWidth: Theme.buttonWidthLarge

    Button {
      id: btn
      text:  qsTr("Solarize")
      onClicked: {
        Functions.solarizeColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
    }
    Button {
      text:  qsTr("Generate") + " " + qsTr("from") + " " + qsTr("Highlight")
      onClicked: {
        Functions.computeColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
      ButtonLayout.newLine: true
    }
    Button {
      text:  qsTr("Night") + " " + qsTr("Theme")
      onClicked: {
        Functions.nightColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
    }
    Button {
      text:  qsTr("Summer") + " " + qsTr("Theme")
      onClicked: {
        Functions.dayColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
      ButtonLayout.newLine: true
    }
    Button {
      property int idx: 0
      property int maxidx: 4
      text:  qsTr("R-G Colorblindness" + " #" + (idx + 1) + "/" + maxidx)
      width: Theme.buttonWidthSmall
      onClicked: {
        Functions.blindColors(MyPalette, idx);
        idx = idx+1;
        if (idx >= maxidx) { idx = 0 };
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
        //console.debug("idx: " + idx);
      }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
