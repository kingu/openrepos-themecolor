import QtQuick 2.6
import Sailfish.Silica 1.0
import "functions.js" as Functions
import ".."

Row {
  width: parent.width
  anchors.horizontalCenter: parent.horizontalCenter

  property color tint: "#10ff0000" // red, 1/10th opacity
  property real factor: 1.1
  Button {
    id: btn
    width: parent.width / 3
    text:  qsTr("Tint") + " " + qsTr("Red")
    color: Qt.tint(MyPalette.primaryColor, "#40ff0000");
    onClicked: {
      Functions.tintColors(MyPalette, tint);
    }
  }
  Button {
    width: btn.width
    text:  qsTr("Darken")
    color: Qt.darker(MyPalette.primaryColor, factor);
    onClicked: {
      Functions.darkenColors(MyPalette, factor);
    }
  }
  Button {
    width: btn.width
    text:  qsTr("Brighten")
    color: Qt.lighter(MyPalette.primaryColor, factor * 2);
    onClicked: {
      Functions.lightenColors(MyPalette, factor);
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
