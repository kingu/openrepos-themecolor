import QtQuick 2.6
import Sailfish.Silica 1.0
import "functions.js" as Functions
import ".."

Row {
    id: row
    anchors.horizontalCenter: parent.horizontalCenter
    width: parent.width
    height: btn.height
    spacing: 0

    Button {
      id: btn
      width: parent.width / 3 - Theme.paddingSmall
      text:  qsTr("Random")
      color: Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
      onClicked: {
        Functions.rndColors(MyPalette);
        color = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
        generatorRemorse.execute( qsTr("Generated") +  " " + text + "…", function(){}, 2400 )
      }
    }
    Button {
      width: btn.width
      text: ( Theme.colorScheme == Theme.LightOnDark ) ? qsTr("Bright") : qsTr("Dark")
      color: {
        var cmax = ( Theme.colorScheme == Theme.LightOnDark ) ? 1.0 : 0.7;
        var cmin = ( Theme.colorScheme == Theme.LightOnDark ) ? 0.3 : 0.0;
        Qt.rgba(Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, Math.random() * (cmax - cmin) + cmin, MyPalette.primaryColor.a);
      }
      onClicked: {
        Functions.rndBrightOrDark(MyPalette)
        color = Qt.rgba(Math.random(), Math.random(), Math.random(), MyPalette.primaryColor.a);
        generatorRemorse.execute( qsTr("Generated") +  " " + text + "…", function(){}, 2400 )
      }
    }
    Button {
      width: btn.width
      text:  qsTr("Gray")
      color: ( Theme.colorScheme == Theme.LightOnDark ) ? Theme.highlightFromColor("lightgray", Theme.colorScheme) : Theme.highlightFromColor("dimgray", Theme.colorScheme)
      onClicked: {
        Functions.rndGray(MyPalette)
        color = Theme.highlightFromColor( MyPalette.primaryColor, Theme.colorScheme);
        generatorRemorse.execute( qsTr("Generated") +  " " + text + "…", function(){}, 2400 )
      }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
