pragma Singleton
import QtQuick 2.6

QtObject {
  // these are filled in by spec file:
  readonly property var version: 0
  readonly property var release: 0
  default property string versionstring: version + "-" + release
  readonly property string appname: "openrepos-themecolor"
  readonly property string displayName: qsTr("ThemeColor")
  readonly property string builderDisplayName: qsTr("ThemeColor® RPM Builder™")
  readonly property string domain: "org.nephros.sailfish"
  //readonly property string organization: this.appname
  readonly property string organization: "."
  readonly property string copyright: "2021 Peter G. (nephros) <" + this.email + ">"
  readonly property string email: "sailfish@nephros.org"
  readonly property string license: "MIT License"
  readonly property string licenseurl: "https://opensource.org/licenses/MIT"
  readonly property string source: "https://gitlab.com/nephros/openrepos-themecolor/"
  readonly property string transurl: "https://hosted.weblate.org/git/theme-color/app"
  // TODO: further complicate things by keeping persons in another data structure
  readonly property ListModel translators: ListModel {
       ListElement { langid: "en_IE";    langname: "English";
            persons:  "Peter G. (nephros)"
       }
       ListElement { langid: "de";    langname: "German";
            persons: 'Peter G. (nephros)'
       }
       ListElement { langid: "nb_NO"; langname: "Norwegian Bokmål";
            persons:  'Allan Nordhøy (kingu)'
       }
       ListElement { langid: "es"; langname: "Spanish";
            persons: 'Carmen F. B (carmenfdez)'
       }
       ListElement { langid: "sv"; langname: "Swedish";
            persons: 'Åke Engelbrektson (eson)'
       }
       ListElement { langid: "zh_CN"; langname: "Simplified Chinese";
            persons: 'Rui Kon (dashinfantry)'
       }
   }
}

// vim: expandtab ts=4 st=4 filetype=javascript
