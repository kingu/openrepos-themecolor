import QtQuick 2.6
import QtQml.Models 2.1
import Sailfish.Silica 1.0
import "../components"
Row {
    //width: page.isLandscape ? parent.width - Theme.itemSizeLarge * 3 : parent.width - Theme.horizontalPageMargin * 2
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    leftPadding: Theme.horizontalPageMargin + Theme.paddingMedium
    rightPadding: Theme.paddingSmall
    spacing: 0

    Image {
      id: dot
      source: "image://theme/icon-m-dot?" + MyPalette.highlightColor
      cache:false
    }
    Slider {
        id: highlightSlider
        width: parent.width - dot.width - Theme.paddingSmall
        minimumValue: 0.0
        maximumValue: 0.9999
        stepSize: 1/359
        value: MyPalette.highlightColor.hsvHueF ? MyPalette.highlightColor.hsvHueF : 0.5
        color: Theme.highlightFromColor(Qt.hsva(value, 1.0, 0.5, 1.0), MyPalette.colorScheme)
        property real hue

        onValueChanged: {
          hue = value
          highlightSlider.enabled =  false
          MyPalette.highlightColor = Theme.highlightFromColor(Qt.hsva(hue, 1.0, 0.5, 1.0), MyPalette.colorScheme)
          MyPalette.primaryColor =  (Theme.colorScheme == Theme.LightOnDark) ? "#FFFFFFFF" : "#FF000000"
          MyPalette.secondaryColor =  (Theme.colorScheme == Theme.LightOnDark) ? "#B0FFFFFF" : "#B0000000"
          MyPalette.secondaryHighlightColor = Theme.secondaryHighlightFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
          MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
          MyPalette.highlightDimmerColor = Theme.highlightDimmerFromColor( MyPalette.highlightColor, MyPalette.colorScheme );
          highlightSlider.enabled =  true
        }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
