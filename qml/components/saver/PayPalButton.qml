import QtQuick 2.6
import Sailfish.Silica 1.0

Button {
    property int blocked
    Image {
        id: ppimg
        source: parent.enabled ? "images/pp.png" : "image://theme/icon-s-checkmark?" + Theme.highlightDimmerFromColor(Theme.presenceColor(Theme.PresenceAvailable), Theme.colorScheme)
        sourceSize.height: Theme.iconSizeMedium
        fillMode: Image.PreserveAspectFit
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.left
    }
    TouchBlocker {anchors.fill: parent; visible: blocked}
}

// vim: expandtab ts=4 st=4 filetype=javascript
