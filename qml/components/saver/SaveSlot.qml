import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Column {
    property bool ambience : false
    property int idx

    property color p
    property color s
    property color h
    property color sh
    property color hbg
    property color bgg
    property color hdc

    function initThemeCols() {
        p = getThemeProp(idx, "p");
        s = getThemeProp(idx, "s");
        h = getThemeProp(idx, "h");
        sh = getThemeProp(idx, "sh");
        hbg = getThemeProp(idx, "hbg");
        bgg = getThemeProp(idx, "bgg");
        hdc = getThemeProp(idx, "hdc");
    }

    anchors.horizontalCenter: parent.horizontalCenter
    // incorrect for a delegate in ListView:
    //width: parent.width - Theme.itemSizeLarge * 2
    spacing: Theme.paddingMedium

    // onCompleted doesn't suffice for a delegate...
    onVisibleChanged: {
      //console.log("instatiating slot with index " + idx)
      initThemeCols()
    }

    SectionHeader {
      text: ( ambience === true ) ? qsTr("Ambience") + " " + qsTr("Shelf") + " " + (idx + 1) : qsTr("Shelf") + " " + ( idx + 1)
      font.pixelSize: Theme.fontSizeMedium
      color: Theme.secondaryHighlightColor
    }
    Row {
        id: ibrow
        anchors.horizontalCenter: parent.horizontalCenter;
        spacing: Theme.paddingSmall
        ColorJar { col: p  }
        ColorJar { col: s  }
        ColorJar { col: h  }
        ColorJar { col: sh }
        ColorJar { col: ( Theme.colorScheme == Theme.LightOnDark ) ? "white" : "black"; bgcol: hbg }
        ColorJar { col: ( Theme.colorScheme == Theme.LightOnDark ) ? "white" : "black"; bgcol: hdc }
        // primary is fixed as primary when dimmed, highlight otherwise
        // actually Silica uses palette, not Theme here
        //GlowIndicator { color: ( Theme.colorScheme == Theme.LightOnDark ) ? Theme.lightPrimaryColor : Theme.primaryColor; backgroundColor: bgg }
        GlowIndicator { color: ( Theme.colorScheme == Theme.LightOnDark ) ? "#ffffffff" : "#ff000000" ; backgroundColor: bgg }
    }
    ButtonLayout {
      id: btnrow
      anchors.horizontalCenter: parent.horizontalCenter
      rowSpacing: 0
      Button { text: qsTr("Take to Lab");   height: Theme.iconSizeSmall; onClicked: { console.debug("loading " + idx + "…") ; loadTheme(idx) } }
      Button { text: qsTr("Put on Shelf");  height: Theme.iconSizeSmall; onClicked: { console.debug("saving " + idx + "…")  ; saveTheme(idx) ; initThemeCols() } }
    }
}


// vim: expandtab ts=4 st=4 filetype=javascript
