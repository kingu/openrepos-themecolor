import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Column {
     id: textcol
     anchors.horizontalCenter: parent.horizontalCenter
     width: parent.width
     SilicaItem {
         anchors.horizontalCenter: parent.horizontalCenter
         width: parent.width
         height: lblcol0.height
         Column {
           id: lblcol0
           width: parent.width
           spacing: Theme.paddingSmall
           Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Primary Color");   color: MyPalette.primaryColor }
           Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Secondary Color"); color: MyPalette.secondaryColor }
           Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Highlight Color"); color: MyPalette.highlightColor }
           Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Secondary Highlight Color");   color: MyPalette.secondaryHighlightColor }
           Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Error Color");     color: MyPalette.errorColor}
         }
     }
     /* ************* Background Colors *****************/
     SilicaItem {
         anchors.horizontalCenter: parent.horizontalCenter
         width: parent.width
         height: lblcol1.height
         Column {
           id: lblcol1
           width: parent.width
           spacing: Theme.paddingSmall
           Label {
             anchors.horizontalCenter: parent.horizontalCenter
             horizontalAlignment: Text.AlignHCenter
             text: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Highlight Background Color")
             color: MyPalette.primaryColor
           }
           Label {
             anchors.horizontalCenter: parent.horizontalCenter
             horizontalAlignment: Text.AlignHCenter
             text: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Highlight Background Color")
             color: MyPalette.secondaryColor
             font.pixelSize: Theme.fontSizeSmall
           }
         }
         Separator {
           horizontalAlignment: Qt.AlignHCenter
           color: MyPalette.highlightBackgroundColor
           anchors.fill: parent
         }
     }
     SilicaItem {
         anchors.horizontalCenter: parent.horizontalCenter
         width: parent.width
         height: lblcol2.height
         Column {
           id: lblcol2
           width: parent.width
           spacing: Theme.paddingSmall
           Label {
             anchors.horizontalCenter: parent.horizontalCenter
             horizontalAlignment: Text.AlignHCenter
             text: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Overlay Background Color")
             color: MyPalette.primaryColor
           }
           Label {
             anchors.horizontalCenter: parent.horizontalCenter
             horizontalAlignment: Text.AlignHCenter
             text: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Overlay Background Color")
             color: MyPalette.secondaryColor
             font.pixelSize: Theme.fontSizeSmall
           }
         }
         Separator {
           horizontalAlignment: Qt.AlignHCenter
           color: MyPalette.overlayBackgroundColor
           anchors.fill: parent
         }
     }
     SilicaItem {
         anchors.horizontalCenter: parent.horizontalCenter
         width: parent.width
         height: lblcol3.height
         Column {
           id: lblcol3
           width: parent.width
           spacing: Theme.paddingSmall
           Label {
             anchors.horizontalCenter: parent.horizontalCenter
             horizontalAlignment: Text.AlignHCenter
             text: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Dim Highlight Color")
             color: MyPalette.primaryColor
           }
           Label {
             anchors.horizontalCenter: parent.horizontalCenter
             horizontalAlignment: Text.AlignHCenter
             text: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Dim Highlight Color")
             color: MyPalette.secondaryColor
             font.pixelSize: Theme.fontSizeSmall
           }
         }
         Separator {
           horizontalAlignment: Qt.AlignHCenter
           color: MyPalette.highlightDimmerColor
           //color: Theme.highlightDimmerFromColor(MyPalette.highlightColor, MyPalette.colorScheme)
           anchors.fill: parent
         }
     }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
