import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

SilicaItem {
    Image {
        id: showbg
        width: parent.width
        height: parent.height
        anchors.horizontalCenter: parent.horizontalCenter

        cache: true
        source: "file://" + imagepath.value
        scale: 0.8
        opacity: 1.0
        fillMode: Image.PreserveAspectFit
    }
    Rectangle {
        id: firstrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: showbg.left
        height: showbg.height
        width: showbg.width / 5
        color: MyPalette.overlayBackgroundColor
        opacity: Theme.opacityHigh
        radius: Theme.paddingLarge
    }
    Rectangle {
        id: secrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: firstrec.right
        height: showbg.height
        width: firstrec.width
        color: MyPalette.overlayBackgroundColor
        opacity: Theme.opacityLow
        radius: Theme.paddingLarge
    }
    Rectangle {
        id: trdrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: secrec.right
        height: showbg.height
        width: firstrec.width
        color: MyPalette.overlayBackgroundColor
        opacity: Theme.opacityFaint
        radius: Theme.paddingLarge
    }
    Rectangle {
        id: fourthrec
        anchors.verticalCenter: showbg.verticalCenter
        anchors.left: trdrec.right
        height: showbg.height
        width: firstrec.width
        color: MyPalette.highlightBackgroundColor
        opacity: Theme.highlightBackgroundOpacity
        radius: Theme.paddingLarge
    }
    Column {
        id: showcol
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        spacing: Theme.paddingMedium

        ShowRoomText { visible: mode == 0}
        ShowRoomUI { visible: mode == 1}
    }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
