import QtQuick 2.6
import Sailfish.Silica 1.0

TextField {
  property color col
  property bool alpha: col.a !== 1
  property string name

  label: qsTr("specify RGB or aRGB value, e.g.") + " " + col + ", " + qsTr("(the # is optional)")
  labelVisible: true
  placeholderText: name + ": " + col
  font.pixelSize: Theme.fontSizeLarge
  focus: true
  inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
  validator: RegExpValidator {
         regExp: alpha ? /^[#]{0,1}[0-9a-f]{6,8}/i : /^[#]{0,1}[0-9a-f]{6}/i
  }
  // not in SFOS 3.4.x
  //rightItem: ColorIndicator {col: col}

  onFocusChanged: {
    if ( focus ) {
      if ( text.length === 0 ) {
        text = "#"
      }
    } else {
      var startsWith = /^#/;
      if ( ! startsWith.test(text) ) {
          text = text.replace(/^/, "#")
      }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
