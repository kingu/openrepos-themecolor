import QtQuick 2.6
import Sailfish.Silica 1.0

Item {
  property alias color: _colrect.color
  height: Theme.fontSizeTiny;
  Rectangle {
    id: _colrect
    anchors.horizontalCenter: parent.horizontalCenter
    width: parent.width - Theme.paddingSmall * 2
    height: parent.height
    border.width: 2
    border.color: Theme.highlightBackgroundFromColor(color, Theme.colorScheme)
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
