pragma Singleton
import QtQuick 2.6
import Sailfish.Silica 1.0

Palette {
  /*
   * these have usable alpha channels:
   */
  readonly property double p_alpha:   this.primaryColor.a
  readonly property double s_alpha:   this.secondaryColor.a
  readonly property double hl_alpha:  this.highlightColor.a
  readonly property double shl_alpha: this.secondaryHighlightColor.a
  // might often times be undefined...
  onHighlightColorChanged: {
    if (typeof(this.highlightDimmerColor) === "undefined")  { this.highlightDimmerColor = Theme.highlightDimmerFromColor(this.highlightColor, this.colorScheme) }
  }

  /*
   * handle global opacity values
   */
  property double highlightBackgroundOpacity: 0.3

  property double genericAlpha: 0.8
  property double opacityFaint: 0.2
  property double opacityLow:   0.4
  property double opacityHigh:  0.6
  property double opacityOverlay: 0.8
  // autogen all the others from generic:
  onGenericAlphaChanged: {
    opacityOverlay = genericAlpha
    opacityHigh    = genericAlpha * (3/4)
    opacityLow     = genericAlpha * (2/4)
    opacityFaint   = genericAlpha * (1/4)
  }

}

// vim: expandtab ts=4 st=4 filetype=javascript
