import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "controls"

Item {

    property var map: []

    property bool initialized: false;
    property bool copy: false;
    Component.onCompleted: {
      var initmap = [
        MyPalette.primaryColor.toString(),
        MyPalette.secondaryColor.toString(),
        MyPalette.highlightColor.toString(),
        MyPalette.secondaryHighlightColor.toString(),
        MyPalette.highlightBackgroundColor.toString()
        //MyPalette.highlightDimmerColor.toString()
      ];
      map = initmap;
      initialized = true;
    }

    onMapChanged: {
      if (!initialized) return;
      console.debug("Map changed.");
      console.debug("Map colors before:\t " + MyPalette.primaryColor  + " " + MyPalette.secondaryColor + " " + MyPalette.highlightColor + " " + MyPalette.secondaryHighlightColor + " " + MyPalette.highlightBackgroundColor+ " " + MyPalette.highlightDimmerColor )
      MyPalette.primaryColor             = map[0];
      MyPalette.secondaryColor           = map[1];
      MyPalette.highlightColor           = map[2];
      MyPalette.secondaryHighlightColor  = map[3];
      MyPalette.highlightBackgroundColor = map[4];
      //MyPalette.highlightDimmerColor     = map[5];
      console.debug("Map colors now:\t " + MyPalette.primaryColor  + " " + MyPalette.secondaryColor + " " + MyPalette.highlightColor + " " + MyPalette.secondaryHighlightColor + " " + MyPalette.highlightBackgroundColor  + " " + MyPalette.highlightDimmerColor )
    }

    function copyColor(a,b) {
      // trick so we emit a signal on change of map:
      var tmp = map;
      var cola = map[a];
      var colb = map[b];
      tmp[b] = cola;
      map = tmp;
     }
    function swapColor(a,b) {
      console.debug("Swap colors in map before:\t " + " " + map[0] + " " + map[1] + " " + map[2] + " " + map[3])
      // trick so we emit a signal on change of map:
      var tmp = map;
      //[ tmp[b], tmp[a] ] = [ map[a], map[b] ];
      var cola = map[a];
      var colb = map[b];
      tmp[b] = cola;
      tmp[a] = colb;
      map = tmp;
      console.debug("Swapped: " + a + "/" + b + " " + map[a] + ", " + map[b]);
      console.debug("Swap colors in map after:\t " + " " + map[0] + " " + map[1] + " " + map[2] + " " + map[3])
    }

    width: parent.width
    height: col.height

    Column {
      id: col
      width: parent.width
      spacing: Theme.paddingSmall
      SectionHeader { id: head; text: copy ? qsTr("Copy") : qsTr("Swap") ; color: Theme.primaryColor}
      Row {
        id: colrow
        width: row.width
        ColRect { width: parent.width / 2; color: map[cola.currentIndex] }
        ColRect { width: parent.width / 2; color: map[colb.currentIndex] }
      }
      Row {
        id: row
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        spacing: 0
        ComboBox {
          id: cola
          width: ( parent.width - button.width ) / 2
          anchors.verticalCenter: parent.verticalCenter
          menu: ContextMenu {
            // TODO: throws errors, but does work
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            MenuItem{ text: qsTr("Primary Color")}
            MenuItem{ text: qsTr("Secondary Color")}
            MenuItem{ text: qsTr("Highlight Color")}
            MenuItem{ text: qsTr("Secondary Highlight Color")}
            MenuItem{ text: qsTr("Highlight Background Color")}
            //MenuItem{ text: qsTr("Highlight Dimmer Color")}
          }
        }
        IconButton {
          id: button
          anchors.verticalCenter: parent.verticalCenter
          height: Theme.iconSizeMedium
          width: Theme.iconSizeMedium
          icon.source: copy ? "image://theme/icon-m-data-upload" : "image://theme/icon-m-data-traffic"
          icon.rotation: 90
          enabled: cola.currentIndex !== colb.currentIndex
          onClicked: copy ? copyColor(cola.currentIndex,colb.currentIndex) : swapColor(cola.currentIndex,colb.currentIndex);
        }
        ComboBox {
          id: colb
          width: ( parent.width - button.width ) / 2
          anchors.verticalCenter: parent.verticalCenter
          menu: ContextMenu {
            // TODO: throws errors, but does work
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            MenuItem{ text: qsTr("Primary Color")}
            MenuItem{ text: qsTr("Secondary Color")}
            MenuItem{ text: qsTr("Highlight Color")}
            MenuItem{ text: qsTr("Secondary Highlight Color")}
            MenuItem{ text: qsTr("Highlight Background Color")}
            //MenuItem{ text: qsTr("Highlight Dimmer Color")}
          }
        }
      }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
