import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "controls"

Item {
  anchors.horizontalCenter: parent.horizontalCenter
  //width: page.isLandscape ? parent.width - Theme.itemSizeLarge * 3 : parent.width - Theme.horizontalPageMargin * 2
  width: parent.width
  height: col.height

  Column {
    id: col
    width: parent.width
    SectionHeader { text: qsTr("Primary Color"); color: Theme.primaryColor}
    ColorTextInput {
      incolor: Theme.primaryColor
      onOutcolorChanged: MyPalette.primaryColor = outcolor
    }
    SectionHeader { text: qsTr("Secondary Color"); color: Theme.primaryColor}
    ColorTextInput {
      incolor: Theme.secondaryColor
      onOutcolorChanged: MyPalette.secondaryColor = outcolor
    }
    SectionHeader { text: qsTr("Highlight Color"); color: Theme.primaryColor}
    ColorTextInput {
      incolor: Theme.highlightColor
      onOutcolorChanged: MyPalette.highlightColor = outcolor
    }
    SectionHeader { text: qsTr("Secondary Highlight Color") ; color: Theme.primaryColor}
    ColorTextInput {
      incolor: Theme.secondaryHighlightColor
      onOutcolorChanged: MyPalette.secondaryHighlightColor = outcolor
    }
    SectionHeader { text: qsTr("Highlight Background Color"); color: Theme.primaryColor}
    ColorTextInput {
      incolor: Theme.highlightBackgroundColor
      onOutcolorChanged: MyPalette.highlightBackgroundColor = outcolor
    }
    SectionHeader { text: qsTr("Background Glow Color"); color: Theme.primaryColor}
    GlowSlider {
    }
   }
}

// vim: expandtab ts=4 st=4 filetype=javascript
