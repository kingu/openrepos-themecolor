/*

MIT License

Copyright (c) 2021 Peter Gantner (nephros)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import "pages"
import "components"

ApplicationWindow {
    id: app

    Component.onCompleted: {
      Qt.application.name = AppInfo.appname;
      Qt.application.organization = AppInfo.organization;
      Qt.application.version = AppInfo.versionstring;
    }

    /*
     * generic interface to SystemD, so we can restart units
     */
    DBusInterface {
        id: systemdbus
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        function restartAmbienced() {
          call('RestartUnit', [ 'ambienced.service', 'replace' ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               )
        }
        function restartLipstick() {
          call('RestartUnit', [ 'lipstick.service', 'replace' ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               )
        }
    }
    /*
     * DBus connection, so we can open system settings
     */
    DBusInterface {
        id: settings
        bus: DBus.SessionBus
        service: "com.jolla.settings"
        path: "/com/jolla/settings/ui"
        iface: "com.jolla.settings.ui"
        //signalsEnabled: true
        function open() {
            typedCall("showPage", { "type": "s", "value": "system_settings/look_and_feel/ambiences" },
                        function (result) { console.debug('DBus call result: ' + result)} ,
                        function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
                     )
        }
    }

    /*
     * DBus connection to ambienced, to save settings to ambience
     */
    DBusInterface {
        id: ambiencedbus
        bus: DBus.SessionBus
        service: "com.jolla.ambienced"
        path: "/com/jolla/ambienced"
        iface: "com.jolla.ambienced"
        signalsEnabled: true
        /*
         * from  http://www.jollausers.com/2013/12/how-to-make-ambiance-wallpapers-for-sailfish-bonus/
         *   method void com.jolla.ambienced.createAmbience(QString url)
         *      where url is a real one or a file:/// to an image
         *   method void com.jolla.ambienced.saveAttributes(int contentType, qlonglong contentId, QVariantMap args)
         */
        function saveAmbience() {
          call('saveAttributes', [ ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
              )
        }
        function makeAmbience() {
          call('createAmbience', [ ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               );
        }
        // signal handler to detect ambience change
        function contentChanged() {
          console.debug("ambienced signalled");
          colorsInitialized = false; initColors();
        }
    }

    // used to detect Gemini
    ConfigurationValue {
        id: devicemodel
        key: "/desktop/lipstick-jolla-home/model"
    }
    // to show the background image in ShowRoom, should probably be moved there...
    ConfigurationValue {
        id: imagepath
        key: "/desktop/jolla/background/portrait/home_picture_filename"
        // now handled by signal above
        //onValueChanged: { colorsInitialized = false; initColors() }
    }

    /*
     * the main theme saving interface
     */
    ConfigurationGroup {
      id: conf
      path: "/desktop/jolla/theme/color"
      synchronous: false
      property color primary
      property color secondary
      property color highlight
      property color secondaryHighlight
      property color highlightBackground
      property color highlightDimmer
      property color backgroundGlow
      property double opacityFaint
      property double opacityLow
      property double opacityHigh
      property double opacityOverlay
     }

    property bool colorsInitialized: false

    allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

    function initColors() {
      if ( !colorsInitialized ) {
         // dump all Theme colors:
         console.info("Ambience/System Theme colors:\n" +
             "\tprimaryColor: " + Theme.primaryColor + "\n" +
             "\tsecondaryColor: " + Theme.secondaryColor + "\n" +
             "\thighlightColor: " + Theme.highlightColor + "\n" +
             "\tsecondaryHighlightColor: " + Theme.secondaryHighlightColor + "\n" +
             "\thighlightBackgroundColor: " + Theme.highlightBackgroundColor + "\n" +
             "\thighlightDimmerColor: " + Theme.highlightDimmerColor + "\n" +
             "\toverlayBackgroundColor: " + Theme.overlayBackgroundColor + "\n" +
             "\tbackgroundGlowColor: " + Theme.backgroundGlowColor + "\n" +
             "\t_wallpaperOverlayColor: " + Theme._wallpaperOverlayColor + "\n" +
             "\t_coverOverlayColor: " + Theme._coverOverlayColor + "\n"
             );


         MyPalette.primaryColor   = conf.primary;
         MyPalette.secondaryColor = conf.secondary;
         MyPalette.highlightColor = conf.highlight;
         MyPalette.secondaryHighlightColor = conf.secondaryHighlight;
         // ??? this sometimes dos not exist? why?
         //( typeof(conf.secondaryHighlight)  === "undefined" ) ? MyPalette.secondaryHighlightColor = Theme.secondaryHighlightColor   : MyPalette.secondaryHighlightColor = conf.secondaryHighlight;
         // these will not exist at first run/load:
         if ( typeof(MyPalette.colorScheme) === "undefined" ) { MyPalette.colorScheme = Theme.colorScheme }
         ( typeof(conf.highlightBackground) === "undefined" ) ? MyPalette.highlightBackgroundColor = Theme.highlightBackgroundColor : MyPalette.highlightBackgroundColor = conf.highlightBackground;
         ( typeof(conf.highlightDimmer )    === "undefined" ) ? MyPalette.highlightDimmerColor = Theme.highlightDimmerColor         : MyPalette.highlightDimmerColor = conf.highlightDimmer;
         ( typeof(conf.backgroundGlow)      === "undefined" ) ? MyPalette.backgroundGlowColor = Theme.backgroundGlowColor           : MyPalette.backgroundGlowColor = conf.backgroundGlow;

         colorsInitialized = true;
      }
      console.debug("init colors")
    }

    function reloadThemeColors() {
      MyPalette.primaryColor                = Theme.primaryColor;
      MyPalette.secondaryColor              = Theme.secondaryColor;
      MyPalette.highlightColor              = Theme.highlightColor;
      MyPalette.secondaryHighlightColor     = Theme.secondaryHighlightColor;
      MyPalette.highlightBackgroundColor    = Theme.highlightBackgroundColor;
      MyPalette.highlightDimmerColor        = Theme.highlightDimmerColor;
      MyPalette.backgroundGlowColor         = Theme.backgroundGlowColor
    }
    function applyThemeColors() {
      conf.primary              = MyPalette.primaryColor;
      conf.secondary            = MyPalette.secondaryColor;
      conf.highlight            = MyPalette.highlightColor;
      conf.secondaryHighlight   = MyPalette.secondaryHighlightColor;
      conf.highlightBackground  = MyPalette.highlightBackgroundColor;
      conf.highlightDimmer      = MyPalette.highlightDimmerColor;
      conf.backgroundGlow       = MyPalette.backgroundGlowColor;
      //conf.opacityFaint         = MyPalette.opacityFaint;
      //conf.opacityLow           = MyPalette.opacityLow;
      //conf.opacityHigh          = MyPalette.opacityHigh;
      //conf.opacityOverlay       = MyPalette.opacityOverlay;
      conf.sync();
      // these are read-only
      //Theme.opacityFaint        = MyPalette.opacityFaint;
      //Theme.opacityLow          = MyPalette.opacityLow;
      //Theme.opacityHigh         = MyPalette.opacityHigh;
      //Theme.opacityOverlay      = MyPalette.opacityOverlay;
    }
    function restartAmbienced() { systemdbus.restartAmbienced() }
    function restartLipstick() { systemdbus.restartLipstick() }
    function resetDconf() {
      conf.clear();
      conf.primary            = (Theme.colorScheme == Theme.LightOnDark) ? "#FFFFFFFF" : "#FF000000";
      conf.secondary          = (Theme.colorScheme == Theme.LightOnDark) ? "#B0FFFFFF" : "#B0000000";
      conf.highlight          = Theme.highlightColor;
      conf.secondaryHighlight = Theme.secondaryHighlightColor;
      conf.sync();
    }

    initialPage: Component { FirstPage{} }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    PageBusyIndicator { running: app.status === Component.Loading }

}

// vim: expandtab ts=4 st=4 filetype=javascript
