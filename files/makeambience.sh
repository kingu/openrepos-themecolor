#!/usr/bin/env bash

#set -x
set -e

function error_out() {
  if [ -z "$1" ]; then
	printf "ERROR: something went wrong.\n"
  else
	printf "%s\n" "$1"
  fi
  printf "exit.\n"
  exit 1
}

templates=/usr/share/openrepos-themecolor/templates
#templates=/home/nemo/devel/git/openrepos-themecolor/files
tmpdir=$(mktemp -d)

ambname=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/ambname | sed "s/'//g" )
imgname=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/imgname | sed "s/'//g" )
ambfilename=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/ambfilename | sed "s/'//g" )
ambroot="/usr/share/ambience/ambience-${ambname}"

# put sources
spec=${tmpdir}/SPECS/${ambname}.spec
/usr/bin/install -D  ${templates}/template.spec "$spec"  || exit 1
#install -D  ${templates}/template.ambience "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience"
/usr/bin/install -D -m 644 "${imgname}" "${tmpdir}/BUILDROOT/${ambroot}/images/${ambname}.jpg"  || error_out 
/usr/bin/install -D -m 644 "${ambfilename}" "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience"  || error_out
# pretty print:
/usr/bin/python3 -m json.tool "${ambfilename}" > "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience"  || error_out
sed -i -e "s/@@ambname@@/${ambname}/g"  "${tmpdir}/SPECS/${ambname}.spec"
# replace full path with our name:
sed -i -e "s@\(\"wallpaper\": \"\).*\([,\"]$\)@\1${ambname}.jpg\2@" "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience" || error_out
#sed -i -e "s/@@ambdisplayname@@/${ambname}/g" ${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience
#sed -i -e "s/@@imgname@@/${imgname##*/}/g" ${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience

#tag=$( date "+%y%m%d_%H%M%S")
# this is relatively stupid:
#tag=$(  printf '%.8s' $( date +%s | md5sum | cut -f 1 -d "-") )
tag=$( date "+%m.%d.%H%M")

outdir="${HOME}/Documents/"

if [ ! -w "${outdir}" ] ; then
  error_out "ERROR: output directory ${outdir} does not exist"
fi

echo all set up, trying build...
# build rpm, do not clean sources
pushd "${tmpdir}" >/dev/null
/usr/bin/rpmbuild --quiet --short-circuit --buildroot="${tmpdir}"/BUILDROOT --define "_rpmdir ${tmpdir}/out" --define "ambtag ${tag}" -bb --noclean "$spec" || error_out "rpmbuild failed: $?"
out=$(mv -v "${tmpdir}"/out/noarch/*rpm "${outdir}" | cut -d ">" -f 2 | xargs echo)
popd >/dev/null
# and clean up
rm -rf "${tmpdir}"

echo "Done. RPM built and placed into $out. Launching installer..."
exec /usr/bin/xdg-open "${out}" 
